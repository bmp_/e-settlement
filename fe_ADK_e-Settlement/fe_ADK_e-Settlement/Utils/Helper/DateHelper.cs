﻿using System;
namespace fe_ADK_e_Settlement.Utils.Helper
{
    public class DateHelper
    {
        private static string DATE_TIME_FORMAT = "MM/dd/yyyy HH:mm:ss";

        public DateHelper()
        {
            //Empty Constructor
        }

        public static string ConvertUTCDateTime(string str)
        {
            str = str.Replace("T", " ").Replace("Z", "");

            DateTime strDate = Convert.ToDateTime(str);

            string result = strDate.ToString(DATE_TIME_FORMAT);
            return result;
        }
    }
}
