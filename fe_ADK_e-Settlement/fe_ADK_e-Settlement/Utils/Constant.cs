﻿using System;
namespace fe_ADK_e_Settlement.Utils
{
    public class Constant
    {
        public Constant()
        {
            // Empty Constructor
        }

        public static string API_CORE_OFA_URL = "http://ofadev.id/CoreOFA/api/AccountInquiry/GetAccountInquiry";
        public static string UPLOAD_FOLDER = "C:\\Users\\Ganesh\\Documents\\bmp\\project\\bka\\settlement\\Upload\\";
        //public static string UPLOAD_FOLDER = "/Users/bmp/Documents/Project/BKA/MVC/settlement/Upload/";
        public static string DEFAULT_PAGE_SIZE = "10";
        public static string GITHUB_ENTRY = @"";

        public static string USER_API_GW = "http://localhost/settlementuserapi";
        public static string API_GET_ADK_USER_PROCESS = USER_API_GW + "/user/get-all-user-adk";
        public static string API_ADK_GET_TOKEN_PROCESS = USER_API_GW + "/user/get-token-adk";

        public static string MASTER_API_GW = "http://localhost/settlementmasterapi";
        public static string API_MASTER_RETRIEVE_JENIS_TRANSAKSI = MASTER_API_GW + "/master-jenis-transaksi/get-all";
        public static string API_MASTER_RETRIEVE_ONE_JENIS_TRANSAKSI = MASTER_API_GW + "/master-jenis-transaksi/get-one";
        public static string API_MASTER_CREATE_JENIS_TRANSAKSI = MASTER_API_GW + "/master-jenis-transaksi/create";
        public static string API_MASTER_UPDATE_JENIS_TRANSAKSI = MASTER_API_GW + "/master-jenis-transaksi/update";
        public static string API_MASTER_DELETE_JENIS_TRANSAKSI = MASTER_API_GW + "/master-jenis-transaksi/delete";
        public static string API_MASTER_RETRIEVE_ALL_LOOKUP = MASTER_API_GW + "/master-lookup/get-all";
        public static string API_MASTER_RETRIEVE_LOOKUP_BY_TYPE = MASTER_API_GW + "/master-lookup/get-by-tipe";
        public static string API_MASTER_CREATE_LOOKUP = MASTER_API_GW + "/master-lookup/create";
        public static string API_MASTER_GET_ONE_LOOKUP = MASTER_API_GW + "/master-lookup/get-one";
        public static string API_MASTER_UPDATE_LOOKUP = MASTER_API_GW + "/master-lookup/update";
        public static string API_MASTER_DELETE_LOOKUP = MASTER_API_GW + "/master-lookup/delete";
        public static string API_MASTER_CREATE_TIME_PROCESS = MASTER_API_GW + "/master-time-proses/create";
        public static string API_MASTER_RETRIEVE_ONE_TIME_PROCESS = MASTER_API_GW + "/master-time-proses/get-one";
        public static string API_MASTER_RETRIEVE_ALL_TIME_PROCESS = MASTER_API_GW + "/master-time-proses/get-by-jenis-transaksi";
        public static string API_MASTER_UPDATE_TIME_PROCESS = MASTER_API_GW + "/master-time-proses/update";
        public static string API_MASTER_DELETE_TIME_PROCESS = MASTER_API_GW + "/master-time-proses/delete";

        public static string CREATE_MEMO_API_GW = "http://localhost/settlementmemoapi";
        public static string API_MEMO_RETRIEVE_ID_URL = CREATE_MEMO_API_GW + "/create-memo/get-new-idmemo";
        public static string API_MEMO_FILE_UPLOAD_URL = CREATE_MEMO_API_GW + "/create-memo/upload";
        public static string API_MEMO_RETRIEVE_UNIT_TUJUAN_URL = CREATE_MEMO_API_GW + "/create-memo/get-unit-tujuan-parent";
        public static string API_MEMO_RETRIEVE_BOOKING_OFFICE_URL = CREATE_MEMO_API_GW + "/create-memo/get-cabang-booking-office";
        public static string API_MEMO_RETRIEVE_UNIT_TUJUAN_DEBITUR_URL = CREATE_MEMO_API_GW + "/create-memo/get-unit-tujuan-debitur";
        public static string API_MEMO_RETRIEVE_CHECKER_URL = CREATE_MEMO_API_GW + "/create-memo/get-list-checker";
        public static string API_MEMO_RETRIEVE_APPROVER_URL = CREATE_MEMO_API_GW + "/create-memo/get-list-approver";
        public static string API_MEMO_GET_ACCOUNT_URL = CREATE_MEMO_API_GW + "/create-memo/get-account/{rekening}";//not implemented
        public static string API_MEMO_CREATE_URL = CREATE_MEMO_API_GW + "/create-memo/create";

    }
}
