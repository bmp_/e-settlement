using Microsoft.Extensions.Logging;

namespace fe_ADK_e_Settlement.Controllers.utils
{
    public class ApplicationLogging
    {
        public static ILoggerFactory LoggerFactory {get;} = new LoggerFactory();
        public static ILogger CreateLogger<T>() =>
        LoggerFactory.CreateLogger<T>();
        
    }
}