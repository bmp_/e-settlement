
var ROOT_PATH = "settlementui";
var BASE_URL = '/' + ROOT_PATH;
//var BASE_URL = '';


$(document).ready(function () {
  $("form").bind("keypress", function (e) {
    if (e.keyCode == 13) {
      return false;
    }
  });

  $(".input-datepicker-close")
    .datepicker({ weekStart: 1 })
    .on("changeDate", function (e) {
      $(this).datepicker("hide");
    });

  var state = Cookies.get("state");
  console.log(state);
});

function notifySuccess(data) {
  var growlType = "success";

  $.bootstrapGrowl("<h4>Success!</h4> <p>" + data + "</p>", {
    type: growlType,
    delay: 2500,
    allow_dismiss: true,
  });
}

function notifyInfo(data) {
  var growlType = "info";

  $.bootstrapGrowl("<h4>Info!</h4> <p>" + data + "</p>", {
    type: growlType,
    delay: 2500,
    allow_dismiss: true,
  });
}

function notifyWarning(data) {
  var growlType = "warning";

  $.bootstrapGrowl("<h4>Warning!</h4> <p>" + data + "</p>", {
    type: growlType,
    delay: 2500,
    allow_dismiss: true,
  });
}

function notifyError(data) {
  var growlType = "danger";

  $.bootstrapGrowl("<h4>Error!</h4> <p>" + data + "</p>", {
    type: growlType,
    delay: 2500,
    allow_dismiss: true,
  });
}

function session() {
  $.ajax({
      url: BASE_URL +"/Login/Session",
    dataType: "json",
    method: "GET",
    success: function (response, textStatus, xhr) {
      switch (xhr.status) {
        case 200:
          break;
        case 401:
          notifyError(response.message);
              window.location.href = BASE_URL+"/login";
          break;
        case 400:
          notifyError(response.message);
              window.location.href = BASE_URL +"/login";
          break;
        default:
      }
    },
    error: function (error, textStatus, xhr) {
      console.log(xhr.status);
      notifyError(error.statusText);
        window.location.href = BASE_URL +"/login";
    },
  });
}

function logout() {
  $.ajax({
      url: BASE_URL +"/Login/Logout",
    dataType: "json",
    method: "POST",
    success: function (response, textStatus, xhr) {
      if (xhr.status != 200) {
        notifyError(response.message);
      }
        window.location.href = BASE_URL +"/login";
      Cookies.set("state", "", { sameSite: "strict" });
      Cookies.set("token", "", { sameSite: "strict" });
      Cookies.set("role", "", { sameSite: "strict" });
    },
    error: function (error) {
        notifyError(error.statusText);
        window.location.href = BASE_URL + "/login";
    },
  });
}
