$(document).ready(function () {
});

function doLogin() {
  var npp = document.getElementById("val_select_user").value;
  $.ajax({
      url: BASE_URL +"/Login/AuthenticateUser",
    dataType: "json",
    method: "POST",
    contentType: "application/json",
    data: JSON.stringify({
      npp: npp,
    }),
    success: function (response, textStatus, xhr) {
      if (response.status == 200) {
          window.location.href = BASE_URL +"/Home";
        Cookies.set("state", "Authenticated", { sameSite: "strict" });
        Cookies.set("token", response.data.token, { sameSite: "strict" });
        Cookies.set("role", response.data.eSettRole, { sameSite: "strict" });
      } else {
        notifyError(response.message);
      }
    },
    error: function (error) {
      notifyError(error.statusText);
    },
  });
}
