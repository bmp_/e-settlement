﻿$(document).ready(function () {

    // dummy
    var roles = Cookies.get("role");
    console.log(roles);

    switch (roles) {
        case "10":
            document.getElementById("Menu").remove();
            document.getElementById("Inbox").remove();
            document.getElementById("Input").remove();
            document.getElementById("Checker").remove();
            document.getElementById("Approver").remove();
            document.getElementById("Distribusi").remove();
            document.getElementById("Monitoring").remove();
            document.getElementById("Tracking").remove();
            break;
        case "21":
            document.getElementById("inputAdk").remove();
            document.getElementById("inputSbo").remove();

            document.getElementById("Checker").remove();
            document.getElementById("Approver").remove();
            document.getElementById("Distribusi").remove();

            document.getElementById("Admin").remove();
            document.getElementById("Setting").remove();
            break;
        case "22":
            document.getElementById("checkerAdk").remove();

            document.getElementById("Input").remove();
            document.getElementById("Inbox").remove();
            document.getElementById("Approver").remove();
            document.getElementById("Distribusi").remove();

            document.getElementById("Admin").remove();
            document.getElementById("Setting").remove();
            break;
        case "23":
            document.getElementById("approveAdk").remove();

            document.getElementById("Input").remove();
            document.getElementById("Inbox").remove();
            document.getElementById("Checker").remove();
            document.getElementById("Distribusi").remove();

            document.getElementById("Admin").remove();
            document.getElementById("Setting").remove();
            break;
        case "30":
            document.getElementById("distribusiPengelola").remove();
            document.getElementById("distribusiPenyelia").remove();

            document.getElementById("Input").remove();
            document.getElementById("Inbox").remove();
            document.getElementById("Checker").remove();
            document.getElementById("Approver").remove();

            document.getElementById("Admin").remove();
            document.getElementById("Setting").remove();
            break;
        case "31":
            document.getElementById("inputUnitBisnis").remove();
            document.getElementById("inputSbo").remove();

            document.getElementById("Inbox").remove();
            document.getElementById("Checker").remove();
            document.getElementById("Approver").remove();
            document.getElementById("Distribusi").remove();

            document.getElementById("Admin").remove();
            document.getElementById("Setting").remove();
            break;
        case "32":
            document.getElementById("checkerUnitBisnis").remove();

            document.getElementById("Input").remove();
            document.getElementById("Inbox").remove();
            document.getElementById("Approver").remove();
            document.getElementById("Distribusi").remove();

            document.getElementById("Admin").remove();
            document.getElementById("Setting").remove();
            break;
        case "33":
            document.getElementById("approveUnitBisnis").remove();

            document.getElementById("Input").remove();
            document.getElementById("Inbox").remove();
            document.getElementById("Checker").remove();
            document.getElementById("Distribusi").remove();

            document.getElementById("Admin").remove();
            document.getElementById("Setting").remove();
            break;
        case "40":
            document.getElementById("distribusiAdk").remove();
            document.getElementById("distribusiPenyelia").remove();

            document.getElementById("Input").remove();
            document.getElementById("Inbox").remove();
            document.getElementById("Checker").remove();
            document.getElementById("Approver").remove();

            document.getElementById("Admin").remove();
            document.getElementById("Setting").remove();
            break;
        case "41":
            document.getElementById("distribusiAdk").remove();
            document.getElementById("distribusiPengelola").remove();

            document.getElementById("Input").remove();
            document.getElementById("Inbox").remove();
            document.getElementById("Checker").remove();
            document.getElementById("Approver").remove();

            document.getElementById("Admin").remove();
            document.getElementById("Setting").remove();
            break;
        case "42":
            document.getElementById("inputAdk").remove();
            document.getElementById("inputUnitBisnis").remove();
            
            document.getElementById("Inbox").remove();
            document.getElementById("Checker").remove();
            document.getElementById("Approver").remove();
            document.getElementById("Distribusi").remove();

            document.getElementById("Admin").remove();
            document.getElementById("Setting").remove();
            break;
        default:
        // code block
    }

    var currentPage = window.location.pathname.split('/').slice(0);
    console.log(currentPage);
    if (currentPage[1] === ROOT_PATH && currentPage[2] === 'Home') {
        var element = document.getElementById("Index");
        element.classList.add("mm-active");
    }
    else if (currentPage[2] === 'Inbox' && currentPage[2] === 'inbox-penolakan') {
        var element = document.getElementById("Inbox");
        element.classList.add("mm-active");
    }
    else if (currentPage[2] === 'inputer' && currentPage[3] === 'unit-bisnis') {
        var element = document.getElementById("Input");
        var subElement = document.getElementById("inputNav");
        var page = document.getElementById("inputUnitBisnis");
        element.classList.add("mm-active");
        subElement.classList.add("mm-collapse", "mm-show");
        page.classList.add("mm-active");
    }
    else if (currentPage[2] === 'inputer' && currentPage[3] === 'Adk') {
        var element = document.getElementById("Input");
        var subElement = document.getElementById("inputNav");
        var page = document.getElementById("inputAdk");
        element.classList.add("mm-active");
        subElement.classList.add("mm-collapse", "mm-show");
        page.classList.add("mm-active");
    }
    else if (currentPage[2] === 'inputer' && currentPage[3] === 'Sbo') {
        var element = document.getElementById("Input");
        var subElement = document.getElementById("inputNav");
        var page = document.getElementById("inputSbo");
        element.classList.add("mm-active");
        subElement.classList.add("mm-collapse", "mm-show");
        page.classList.add("mm-active");
    }
    else if (currentPage[2] === 'Checker' && currentPage[3] === 'unit-bisnis') {
        var element = document.getElementById("Checker");
        var subElement = document.getElementById("checkNav");
        var page = document.getElementById("checkerUnitBisnis");
        element.classList.add("mm-active");
        subElement.classList.add("mm-collapse", "mm-show");
        page.classList.add("mm-active");
    }
    else if (currentPage[2] === 'Checker' && currentPage[3] === 'Adk') {
        var element = document.getElementById("Checker");
        var subElement = document.getElementById("checkNav");
        var page = document.getElementById("checkerAdk");
        element.classList.add("mm-active");
        subElement.classList.add("mm-collapse", "mm-show");
        page.classList.add("mm-active");
    }
    else if (currentPage[2] === 'Approver' && currentPage[3] === 'unit-bisnis') {
        var element = document.getElementById("Approver");
        var subElement = document.getElementById("approveNav");
        var page = document.getElementById("approveUnitBisnis");
        element.classList.add("mm-active");
        subElement.classList.add("mm-collapse", "mm-show");
        page.classList.add("mm-active");
    }
    else if (currentPage[2] === 'Approver' && currentPage[3] === 'Adk') {
        var element = document.getElementById("Approver");
        var subElement = document.getElementById("approveNav");
        var page = document.getElementById("approveAdk");
        element.classList.add("mm-active");
        subElement.classList.add("mm-collapse", "mm-show");
        page.classList.add("mm-active");
    }
    else if (currentPage[2] === 'Distribusi' && currentPage[3] === 'Adk') {
        var element = document.getElementById("Distribusi");
        var subElement = document.getElementById("distribusiNav");
        var page = document.getElementById("distribusiAdk");
        element.classList.add("mm-active");
        subElement.classList.add("mm-collapse", "mm-show");
        page.classList.add("mm-active");
    }
    else if (currentPage[2] === 'Distribusi' && currentPage[3] === 'Pengelola') {
        var element = document.getElementById("Distribusi");
        var subElement = document.getElementById("distribusiNav");
        var page = document.getElementById("distribusiPengelola");
        element.classList.add("mm-active");
        subElement.classList.add("mm-collapse", "mm-show");
        page.classList.add("mm-active");
    }
    else if (currentPage[2] === 'Distribusi' && currentPage[3] === 'Penyelia') {
        var element = document.getElementById("Distribusi");
        var subElement = document.getElementById("distribusiNav");
        var page = document.getElementById("distribusiPenyelia");
        element.classList.add("mm-active");
        subElement.classList.add("mm-collapse", "mm-show");
        page.classList.add("mm-active");
    }
    else if (currentPage[1] === ROOT_PATH && currentPage[2] === 'Monitoring') {
        var element = document.getElementById("Monitoring");
        element.classList.add("mm-active");
    }
    else if (currentPage[1] === ROOT_PATH && currentPage[2] === 'tracking-tracing') {
        var element = document.getElementById("Tracking");
        element.classList.add("mm-active");
    }
    else if (currentPage[1] === ROOT_PATH && currentPage[2] === 'setting') {
        var element = document.getElementById("Setting");
        element.classList.add("mm-active");
    }
});