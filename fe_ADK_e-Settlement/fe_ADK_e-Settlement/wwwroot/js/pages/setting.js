$(document).ready(function () {
  session();
  $("#tabel-master-jenis-transaksi").DataTable({});
  generateMasterTimeDT();
  generateMasterLookupDT();
});

function tambahJenisTransaksi() {
  var noTransaksi = document.getElementById("val-add-no-transaksi").value;
  var namaTransaksi = document.getElementById("val-add-nama-transaksi").value;
  $.ajax({
      url: BASE_URL +"/setting/jenis-transaksi",
    dataType: "json",
    method: "POST",
    contentType: "application/json",
    data: JSON.stringify({
        nomor: parseInt(noTransaksi),
      jenisTransaksi: namaTransaksi,
    }),
    success: function (response, textStatus, xhr) {
      if (xhr.status == 200) {
        notifySuccess(response.message);
      } else {
        notifyError(response.message);
      }
      location.reload();
      document.getElementById("val-add-no-transaksi").value = "";
      document.getElementById("val-add-nama-transaksi").value = "";
    },
    error: function (error) {
      location.reload();
      notifyError(error.statusText);
    },
  });
}

function prevDelete(e) {
  var asd = e.id;
  var data = asd.split(",");
  var info = document.getElementById("trx-del-info");
  info.innerHTML =
    "Anda yakin akan menghapus transaksi nomor <b>" +
    data[1] +
    "</b> <b>(" +
    data[2] +
    ")</b>?</p>";

  var idTrx = document.getElementById("trx-del-id");
  idTrx.value = data[0];
}

function DeleteJenisTransaksi() {
  var idTrx = document.getElementById("trx-del-id").value;

  $.ajax({
      url: BASE_URL + "/setting/jenis-transaksi/" + idTrx,
    dataType: "json",
    method: "DELETE",
    success: function (response, textStatus, xhr) {
      if (xhr.status == 200) {
        notifySuccess(response.message);
      } else {
        notifyError(response.message);
      }
      location.reload();
    },
    error: function (error) {
      location.reload();
      notifyError(error.statusText);
    },
  });
}

function prevEdit(e) {
  var asd = e.id;
  var data = asd.split(",");

  var idTrx = document.getElementById("val-edit-id-transaksi");
  idTrx.value = data[0];
  var noTrx = document.getElementById("val-edit-no-transaksi");
  noTrx.value = data[1];
  var namaTrx = document.getElementById("val-edit-nama-transaksi");
  namaTrx.value = data[2];
}

function EditJenisTransaksi() {
  var idTrx = document.getElementById("val-edit-id-transaksi").value;
  var noTrx = document.getElementById("val-edit-no-transaksi").value;
  var namaTrx = document.getElementById("val-edit-nama-transaksi").value;

  $.ajax({
      url: BASE_URL +"/setting/jenis-transaksi",
    dataType: "json",
    method: "PUT",
    contentType: "application/json",
    data: JSON.stringify({
      idTransaksi: idTrx,
        nomor: parseInt(noTrx),
      jenisTransaksi: namaTrx,
    }),
    success: function (response, textStatus, xhr) {
      if (xhr.status == 200) {
        notifySuccess(response.message);
      } else {
        notifyError(response.message);
      }
      location.reload();
      document.getElementById("val-edit-id-transaksi").value = "";
      document.getElementById("val-edit-no-transaksi").value = "";
      document.getElementById("val-edit-nama-transaksi").value = "";
    },
    error: function (error) {
      location.reload();
      notifyError(error.statusText);
    },
  });
}

function getMasterTimeProses() {
  var idTransaksi = document.getElementById("val_tproses_jenis_transaksi");
  var filterButton = document.getElementById("btn_filter_time_proses");
  idTransaksi.setAttribute("disabled", true);
  filterButton.setAttribute("disabled", true);

  var masterTabel = $("#tabel-master-time_proses").DataTable();
  masterTabel.clear();

  $.ajax({
      url: BASE_URL +"/setting/time-proses/all/" + idTransaksi.value,
    dataType: "json",
    method: "GET",
    success: function (response, textStatus, xhr) {
      if (xhr.status == 200) {
        idTransaksi.removeAttribute("disabled");
        filterButton.removeAttribute("disabled");

        var dataList = response.data;
        for (var i = 0; i < dataList.length; i++) {
          var idproses = dataList[i].idProses;
          var namaProses = dataList[i].namaProses;
          var waktuProses = dataList[i].waktu;
          masterTabel.row
            .add([
              dataList[i].nomorUrutJenis,
              dataList[i].namaJenisTransaksi,
              dataList[i].namaProses,
              dataList[i].waktu,
              '<a class="btn btn-lg btn-info active" data-toggle="modal" data-target="#modal-edit-time-process" id="' +
                idproses +
                "," +
                namaProses +
                "," +
                waktuProses +
                '" onclick=prefEditMtp(this)>Edit</a>  ' +
                '<a class="btn btn-lg btn-warning active" data-toggle="modal" data-target="#modal-delete-time-proses" id="' +
                idproses +
                "," +
                namaProses +
                '" onclick=prefDeleteMtp(this)>Delete</a>',
            ])
            .draw();
        }
      } else {
        notifyError(response.message);
        location.reload();
      }
    },
    error: function (error) {
      location.reload();
      notifyError(error.statusText);
    },
  });
}

function generateMasterTimeDT() {
  $("#tabel-master-time_proses").DataTable({
    responsive: true,
    columnDefs: [
      {
        targets: 0,
        className: "dt-body-center",
      },
      {
        targets: 1,
        className: "dt-body-left",
      },
      {
        targets: 2,
        className: "dt-body-left",
      },
      {
        targets: 3,
        className: "dt-body-center",
      },
      {
        targets: 4,
        className: "dt-body-center",
      },
    ],
  });
}

function prefAddMtp() {
  $.ajax({
      url: BASE_URL +"/setting/jenis-transaksi",
    dataType: "json",
    method: "GET",
    success: function (result, statusText, xhr) {
      if (xhr.status == 200) {
        var select = document.getElementById("sel_add_time_process");
        for (var i = 0; i < result.length; i++) {
          select.options[select.options.length] = new Option(
            result[i].nomor + ". " + result[i].jenisTransaksi,
            result[i].idTransaksi
          );
        }
      } else {
        location.reload();
        notifyError("Error");
      }
    },
    error: function (error) {
      location.reload();
      notifyError(error.statusText);
    },
  });
}

function prefEditMtp(e) {
  var asd = e.id;
  var data = asd.split(",");

  var idTrx = document.getElementById("val-edit-id-mtp");
  idTrx.value = data[0];
  var noTrx = document.getElementById("val-edit-nama-mtp");
  noTrx.value = data[1];
  var namaTrx = document.getElementById("val-edit-waktu-mtp");
  namaTrx.value = data[2];
}
function prefDeleteMtp(e) {
  var asd = e.id;
  var data = asd.split(",");
  var info = document.getElementById("mtp-del-info");
  info.innerHTML =
    "Anda yakin akan menghapus waktu proses <b>" + data[1] + "</b>?</p>";

  var idMtp = document.getElementById("mtp-del-id");
  idMtp.value = data[0];
}

function AddTimeProcess() {
  var idTransaksi = document.getElementById("sel_add_time_process").value;
  var namaProses = document.getElementById("val_add_nama_proses").value;
  var waktuProses = document.getElementById("val_add_waktu_proses").value;

  $.ajax({
      url: BASE_URL +"/setting/time-proses",
    dataType: "json",
    method: "POST",
    contentType: "application/json",
    data: JSON.stringify({
      idJenisTransaksi: idTransaksi,
      namaProses: namaProses,
        waktu: parseInt(waktuProses),
    }),
    success: function (response, textStatus, xhr) {
      if (xhr.status == 200) {
        notifySuccess(response.message);
      } else {
        notifyError(response.message);
      }
      location.reload();

      document.getElementById("sel_add_time_process").value = "";
      document.getElementById("val_add_nama_proses").value = "";
      document.getElementById("val_add_waktu_proses").value = "";
    },
    error: function (error) {
      location.reload();
      notifyError(error.statusText);
    },
  });
}

function editTimeProses() {
  var idMtp = document.getElementById("val-edit-id-mtp").value;
  var waktuMtp = document.getElementById("val-edit-waktu-mtp").value;

  $.ajax({
      url: BASE_URL +"/setting/time-proses",
    dataType: "json",
    method: "PUT",
    contentType: "application/json",
    data: JSON.stringify({
      idProses: idMtp,
        waktu: parseInt(waktuMtp),
    }),
    success: function (response, textStatus, xhr) {
      if (xhr.status == 200) {
        notifySuccess(response.message);
      } else {
        notifyError(response.message);
      }
      location.reload();
      document.getElementById("val-edit-id-mtp").value = "";
      document.getElementById("val-edit-nama-mtp").value = "";
      document.getElementById("val-edit-waktu-mtp").value = "";
    },
    error: function (error) {
      location.reload();
      notifyError(error.statusText);
    },
  });
}

function deleteTimeProses() {
  var idMtp = document.getElementById("mtp-del-id").value;
  $.ajax({
      url: BASE_URL +"/setting/time-proses/" + idMtp,
    dataType: "json",
    method: "DELETE",
    success: function (response, textStatus, xhr) {
      if (xhr.status == 200) {
        notifySuccess(response.message);
      } else {
        notifyError(response.message);
      }
      location.reload();
    },
    error: function (error) {
      location.reload();
      notifyError(error.statusText);
    },
  });
}

function AddMasterLookup() {
  var tipe = document.getElementById("val-add-tipe-lookup").value;
  var nama = document.getElementById("val-add-nama-lookup").value;
  var nilai = document.getElementById("val-add-nilai-lookup").value;
  var urutan = document.getElementById("val-add-urutan-lookup").value;

  $.ajax({
      url: BASE_URL +"/setting/lookup",
    dataType: "json",
    method: "POST",
    contentType: "application/json",
    data: JSON.stringify({
      tipe: tipe,
      nama: nama,
        nilai: parseInt(nilai),
        urutan: parseInt(urutan),
    }),
    success: function (response, textStatus, xhr) {
      if (xhr.status == 200) {
        notifySuccess(response.message);
      } else {
        notifyError(response.message);
      }
      location.reload();
    },
    error: function (error) {
      location.reload();
      notifyError(error.statusText);
    },
  });
}

function GetMasterLookup() {
  var filter = document.getElementById("val_filter_lookup").value;
  var value = document.getElementById("val_filter_value_lookup").value;
  var size = document.getElementById("val_filter_size_lookup").value;

  if (filter === "orderBy") {
    GetMasterLookupOrder(value, size);
  } else if (filter === "tipe") {
    GetMasterLookupType(value, size);
  }
}

function GetMasterLookupOrder(filter, size) {
  var masterTabel = $("#tabel-master-lookup").DataTable();
  masterTabel.clear();

  $.ajax({
      url: BASE_URL +"/setting/lookup/order",
    dataType: "json",
    method: "POST",
    contentType: "application/json",
    data: JSON.stringify({
      OrderBy: filter,
      SortBy: "ASC",
      PageNumber: "1",
      PageSize: size,
    }),
    success: function (response, textStatus, xhr) {
      if (xhr.status == 200) {
        var dataList = response.data;
        for (var i = 0; i < dataList.length; i++) {
          var idLookup = dataList[i].idLookup;
          var namaLookup = dataList[i].nama;
          var tipeLookup = dataList[i].tipe;
          var nilaiLookup = dataList[i].nilai;
          var urutanLookup = dataList[i].urutan;
          masterTabel.row
            .add([
              urutanLookup,
              tipeLookup,
              namaLookup,
              nilaiLookup,
              '<a class="btn btn-lg btn-info active" data-toggle="modal" data-target="#modal-edit-lookup" id="' +
                idLookup +
                "," +
                tipeLookup +
                "," +
                namaLookup +
                "," +
                nilaiLookup +
                "," +
                urutanLookup +
                '" onclick=prefEditLookup(this)>Edit</a>  ' +
                '<a class="btn btn-lg btn-warning active" data-toggle="modal" data-target="#modal-delete-lookup" id="' +
                idLookup +
                "," +
                namaLookup +
                '" onclick=prefDeleteLookup(this)>Delete</a>',
            ])
            .draw();
        }
      } else {
        notifyError(response.message);
      }
    },
    error: function (error) {
      notifyError(error.statusText);
    },
  });
}
function GetMasterLookupType(filter, size) {
  var masterTabel = $("#tabel-master-lookup").DataTable();
  masterTabel.clear();

  $.ajax({
      url: BASE_URL +"/setting/lookup/type",
    dataType: "json",
    method: "POST",
    contentType: "application/json",
    data: JSON.stringify({
      OrderBy: "nama",
      SortBy: "ASC",
      PageNumber: "1",
      PageSize: size,
      tipe: filter,
    }),
    success: function (response, textStatus, xhr) {
      if (xhr.status == 200) {
        var dataList = response.data;
        for (var i = 0; i < dataList.length; i++) {
          var idLookup = dataList[i].idLookup;
          var namaLookup = dataList[i].nama;
          var tipeLookup = dataList[i].tipe;
          var nilaiLookup = dataList[i].nilai;
          var urutanLookup = dataList[i].urutan;
          masterTabel.row
            .add([
              urutanLookup,
              tipeLookup,
              namaLookup,
              nilaiLookup,
              '<a class="btn btn-lg btn-info active" data-toggle="modal" data-target="#modal-edit-lookup" id="' +
                idLookup +
                "," +
                tipeLookup +
                "," +
                namaLookup +
                "," +
                nilaiLookup +
                "," +
                urutanLookup +
                '" onclick=prefEditLookup(this)>Edit</a>  ' +
                '<a class="btn btn-lg btn-warning active" data-toggle="modal" data-target="#modal-delete-lookup" id="' +
                idLookup +
                "," +
                namaLookup +
                '" onclick=prefDeleteLookup(this)>Delete</a>',
            ])
            .draw();
        }
      } else {
        notifyError(response.message);
      }
    },
    error: function (error) {
      notifyError(error.statusText);
    },
  });
}

function generateMasterLookupDT() {
  $("#tabel-master-lookup").DataTable({
    responsive: true,
    paging: false,
    info: false,
    sDom: "lrtip",
    columnDefs: [
      {
        targets: 0,
        className: "dt-body-center",
      },
      {
        targets: 1,
        className: "dt-body-left",
      },
      {
        targets: 2,
        className: "dt-body-left",
      },
      {
        targets: 3,
        className: "dt-body-center",
      },
      {
        targets: 4,
        className: "dt-body-center",
      },
    ],
  });
}

function prefEditLookup(e) {
  var asd = e.id;
  var data = asd.split(",");

  document.getElementById("lookup-edit-id").value = data[0];
  document.getElementById("val-edit-tipe-lookup").value = data[1];
  document.getElementById("val-edit-nama-lookup").value = data[2];
  document.getElementById("val-edit-nilai-lookup").value = data[3];
  document.getElementById("val-edit-urutan-lookup").value = data[4];
}
function prefDeleteLookup(e) {
  var asd = e.id;
  var data = asd.split(",");
  var info = document.getElementById("lookup-del-info");
  info.innerHTML =
    "Anda yakin akan menghapus master lookup <b>" + data[1] + "</b>?</p>";

  var idMtp = document.getElementById("lookup-del-id");
  idMtp.value = data[0];
}

function UpdateMasterLookup() {
  var idLookup = document.getElementById("lookup-edit-id").value;
  var tipe = document.getElementById("val-edit-tipe-lookup").value;
  var nama = document.getElementById("val-edit-nama-lookup").value;
  var nilai = document.getElementById("val-edit-nilai-lookup").value;
  var urutan = document.getElementById("val-edit-urutan-lookup").value;

  $.ajax({
      url: BASE_URL +"/setting/lookup",
    dataType: "json",
    method: "PUT",
    contentType: "application/json",
    data: JSON.stringify({
      idLookup: idLookup,
      tipe: tipe,
      nama: nama,
        nilai: parseInt(nilai),
        urutan: parseInt(urutan),
    }),
    success: function (response, textStatus, xhr) {
      if (xhr.status == 200) {
        notifySuccess(response.message);
      } else {
        notifyError(response.message);
      }
      location.reload();
    },
    error: function (error) {
      location.reload();
      notifyError(error.statusText);
    },
  });
}

function DeleteMasterLookup() {
  var idMtp = document.getElementById("lookup-del-id").value;

  $.ajax({
      url: BASE_URL +"/setting/lookup/" + idMtp,
    dataType: "json",
    method: "DELETE",
    success: function (response, textStatus, xhr) {
      if (xhr.status == 200) {
        notifySuccess(response.message);
      } else {
        notifyError(response.message);
      }
      location.reload();
    },
    error: function (error) {
      location.reload();
      notifyError(error.statusText);
    },
  });
}
