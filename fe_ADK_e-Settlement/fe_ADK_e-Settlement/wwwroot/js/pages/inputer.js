﻿$(document).ready(function () {
    generateTimeProsesDt();
    generateID();

    var defaultPage = "1";
    getBookingOffice(defaultPage);
    getUnitTujuanDebitur(defaultPage);

});

function generateID() {
    var maxLength = 16
    var random = '';
    for (var i = 0; i < maxLength; i++) {
        random += Math.floor(Math.random() * 10);
    }
    document.getElementById('val_generate_id').value = random;
}

function getTimeProses() {
    var idTransaksi = document.getElementById("val_tproses_jenis_transaksi");
    var filterButton = document.getElementById("btn_filter_time_proses");
    idTransaksi.setAttribute("disabled", true);
    filterButton.setAttribute("disabled", true);

    var masterTabel = $("#tabel-time_proses").DataTable();
    masterTabel.clear();

    $.ajax({
        url: BASE_URL + "/setting/time-proses/all/" + idTransaksi.value,
        dataType: "json",
        method: "GET",
        success: function (response, textStatus, xhr) {
            if (xhr.status == 200) {
                idTransaksi.removeAttribute("disabled");
                filterButton.removeAttribute("disabled");

                var dataList = response.data;
                for (var i = 0; i < dataList.length; i++) {
                    var idproses = dataList[i].idProses;
                    var namaProses = dataList[i].namaProses;
                    var waktuProses = dataList[i].waktu;
                    masterTabel.row
                        .add([
                            namaProses,
                            '<input type="number" step="any" name="val_time_process"  class= "form-control text-center time-process calculate" value = "' + waktuProses +'" disabled />',
                            '<input type="number" step="any" name="val_total_process" class= "form-control text-center total-process calculate" />',
                            '<p class="text - center sub - total" style="margin-bottom: 0px;" />'
                        ])
                        .draw();
                }
            } else {
                notifyError(response.message);
                location.reload();
            }
        },
        error: function (error) {
            location.reload();
            notifyError(error.statusText);
        },
    });
}
function generateTimeProsesDt() {
    $("#tabel-time_proses").DataTable({
        responsive: true,
        paging: false,
        info: false,
        sDom: "lrtip",
        createdRow: function (row, data, dataIndex) {
            $(row).addClass('transaction');
        },
        columnDefs: [
            {
                targets: 0,
                className: "dt-body-left",
            },
            {
                targets: 1,
                className: "dt-body-center",
            },
            {
                targets: 2,
                className: "dt-body-center",
            },
            {
                targets: 3,
                className: "dt-body-center",
            }
        ],
    });
}

$(".calculate").focusout(
    function () {
        var transaction = $(this).closest('.transaction');
        transaction.find(".net").html('');
        var timeProcess = transaction.find('.time-process').value();
        console.log(timeProcess);
        var totalProcess = transaction.find('.total-process').value();
        console.log(totalProcess);
        if (totalProcess != "") {
            var subTotal = (timeProcess * totalProcess);
            transaction.find(".sub-total").html(Math.round(subTotal * 1000) / 1000);
            console.log(subTotal);
        }
    });

function formatDate(date) {
    var d = new Date(date),
        month = "" + (d.getMonth() + 1),
        day = "" + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [year, month, day].join("-");
}

var rowN = 0;
document.getElementById("fileSelect").onchange = function () {
    var file = this.files[0];
    var table = document.getElementById("fileTable");
    var date = new Date(file.lastModified);
    var lastModifiedDate = ("0" + date.getDate()).slice(-2) + '/' + ("0" + date.getMonth()).slice(-2) + '/' + date.getFullYear() + ' ' + ("0" + date.getHours()).slice(-2) + ':' + ("0" + date.getMinutes()).slice(-2) + ':' + ("0" + date.getSeconds()).slice(-2);
    var size = formatBytes(file.size);

    var fileName = file.name.split('.').slice(0, -1).join('.');
    var convertedFileName = fileName.replace(/ /g, "_");
    convertedFileName = convertedFileName.split(".").join("_");
    convertedFileName = convertedFileName.toLowerCase();
    var currentFile = document.getElementById("upload_status_" + convertedFileName);

    if (currentFile === null) {

        var row = '<tr><td>' + fileName + '</td><td>' + file.name.split('.').pop() + '</td><td>' + size + '</td><td>' + lastModifiedDate + '</td><td><a id="upload_status_' + convertedFileName + '" style="color:white;" data-toggle="tooltip" data-placement="top" title="" data-original-title="rejected"></a></td><td><a id="delete_' + convertedFileName + '" class="btn btn-lg btn-secondary active">Hapus</a></td></tr>'
        table.innerHTML = table.innerHTML + row;

        if (file.size > 2000000) {
            var statusText = document.getElementById("upload_status_" + convertedFileName);
            statusText.innerHTML = "File lebih dari 2MB";
            statusText.classList.add("badge", "badge-danger");
        } else {
            upload(file)
        }
        rowN++;

    }
    else if (currentFile != null) {
        var currentFileStatus = currentFile.classList.contains('badge-secondary');
        if (currentFileStatus === null) {
            var row = '<tr><td>' + fileName + '</td><td>' + file.name.split('.').pop() + '</td><td>' + size + '</td><td>' + lastModifiedDate + '</td><td><a id="upload_status_' + convertedFileName + '" style="color:white;" data-toggle="tooltip" data-placement="top" title="" data-original-title="rejected"></a></td><td><a id="delete_' + convertedFileName + '" class="btn btn-lg btn-secondary active">Hapus</a></td></tr>'
            table.innerHTML = table.innerHTML + row;

            if (file.size > 2000000) {
                var statusText = document.getElementById("upload_status_" + convertedFileName);
                statusText.innerHTML = "File lebih dari 2MB";
                statusText.classList.add("badge", "badge-danger");
            } else {
                upload(file)
            }
            rowN++;
        }
        else if (currentFile != null && currentFileStatus != null) {
            alert("File " + fileName + " sudah ada!!. \nSilahkan hapus file dari list lalu ulangi");
        }
    }

};

function formatBytes(a, b = 2) {
    if (0 === a)
        return "0 Bytes";
    const c = 0 > b ? 0 : b, d = Math.floor(Math.log(a) / Math.log(1024));
    return parseFloat((a / Math.pow(1024, d)).toFixed(c)) + " " + ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"][d]
}

function upload(file) {

    var memoId = document.getElementById("val_hide_memo_id").value;

    var selectFile = document.getElementById("fileSelect");
    var fileName = file.name.split('.').slice(0, -1).join('.');
    var convertedFileName = fileName.replace(/ /g, "_");
    convertedFileName = convertedFileName.split(".").join("_");
    convertedFileName = convertedFileName.toLowerCase();

    var statusText = document.getElementById("upload_status_" + convertedFileName);
    statusText.innerHTML = "Uploading";
    statusText.classList.add("badge", "badge-warning");

    var fd = new FormData();
    selectFile.disabled = true;
    fd.append('file', file);

    $.ajax({
        url: BASE_URL + '/inputer/unit-bisnis/create/upload/' + memoId,
        data: fd,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function (result, st, xhr) {
            if (xhr.status == 200) {
                statusText.innerHTML = result.message;
                statusText.classList.remove("badge-warning")
                statusText.classList.add("badge", "badge-success");
            }
            else {
                statusText.innerHTML = "Failed";
                statusText.classList.remove("badge-warning")
                statusText.classList.add("badge", "badge-danger");
                notifyError(result.message);
            }
            selectFile.disabled = false;
        },
        error: function (error) {
            statusText.innerHTML = "Error";
            statusText.classList.remove("badge-warning")
            statusText.classList.add("badge", "badge-danger");
            notifyError(error.message);
        }
    });
}

function getBankAccount() {
    var value = document.getElementById('val_no_rekening').value;
    document.getElementById("val_nama_debitur").value = "";
    document.getElementById("val_cif").value = "";

    if (value != "" || value != null) {
        $.ajax({
            url: BASE_URL + '/inputer/unit-bisnis/create/get-cif-account/' + value,
            dataType: 'json',
            method: 'GET',
            success: function (result, statusText, xhr) {
                if (xhr.status == 200) {
                    document.getElementById("val_nama_debitur").value = result.data.nama;
                    document.getElementById("val_cif").value = result.data.cif;
                }
                else {
                    notifyError(result.message);
                }
            },
            error: function (error) {
                notifyError(error.message);
            }
        });
    }
}

function getBookingOffice(page) {
    // need validation page number
    var pageNumber = page;

    $.ajax({
        url: BASE_URL + '/inputer/unit-bisnis/create/retrieve-booking-office',
        dataType: 'json',
        method: "POST",
        contentType: "application/json",
        data: JSON.stringify({
            OrderBy: "nama",
            SortBy: "ASC",
            PageNumber: pageNumber,
            PageSize: "10",
        }),

        success: function (result, statusText, xhr) {
            if (xhr.status == 200) {
                var select = document.getElementById("val_booking_officer");
                select.options.length = 0;
                select.options[select.options.length] = new Option('Pilih');
                for (var i = 0; i < result.data.length; i++) {
                    select.options[select.options.length] = new Option(result.data[i].nama, result.data[i].nama);
                }
            }
            else {
                notifyError(result.message);
            }
        },
        error: function (error) {
            notifyError(error.message);
        }
    });

}
function getUnitTujuanDebitur(page) {
    // need validation page number
    var pageNumber = page;

    $.ajax({
        url: BASE_URL + '/inputer/unit-bisnis/create/retrieve-unit-tujuan-debitur',
        dataType: 'json',
        method: "POST",
        contentType: "application/json",
        data: JSON.stringify({
            OrderBy: "nama",
            SortBy: "ASC",
            PageNumber: pageNumber,
            PageSize: "10",
        }),

        success: function (result, statusText, xhr) {
            if (xhr.status == 200) {
                var select = document.getElementById("val_debitur_unit_tujuan");
                select.options.length = 0;
                select.options[select.options.length] = new Option('Pilih');
                for (var i = 0; i < result.data.length; i++) {
                    select.options[select.options.length] = new Option(result.data[i].nama, result.data[i].nama);
                }
            }
            else {
                notifyError(result.message);
            }
        },
        error: function (error) {
            notifyError(error.message);
        }
    });
}

var boPage = 1;
function prefPageBO() {
    if (boPage != 1) {
        boPage--;
        getBookingOffice(boPage.toString());
    }
}

function nextPageBO() {
    boPage ++;
    getBookingOffice(boPage.toString());
}

var utPage = 1;
function prefPageUT() {
    if (utPage != 1) {
        utPage--;
        getUnitTujuanDebitur(utPage.toString());
    }
}

function nextPageUT() {
    utPage++;
    getUnitTujuanDebitur(utPage.toString());

}

function sendRequest(data) {

    // validate trigger using submit, draft, or cancel;\\
     $.ajax({
        url: "createMemo",
        dataType: 'json',
        method: 'POST',
        data: { data
        },
        success: function (result, statusText, xhr) {
            if (xhr.status == 200) {
                notifySuccess(result.message);
                window.location.href = "/Home";
            }
            else {
                notifyError(result.message);
                window.location.href = "/Inputer/UnitBisnis";
            }
        },
        error: function (error) {
            notifyError(error.statusText);
            window.location.href = "/Inputer/UnitBisnis";
        }
    });
}