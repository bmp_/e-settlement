﻿using System;
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Models
{
    public class ViewTableModel
    {
        [JsonProperty(PropertyName = "id")]
        public int id { get; set; }

        [JsonProperty(PropertyName = "unitTujuan")]
        public string unitTujuan { get; set; }

        [JsonProperty(PropertyName = "unitInput")]
        public string unitInput { get; set; }

        [JsonProperty(PropertyName = "namaDebitur")]
        public string namaDebitur { get; set; }

        [JsonProperty(PropertyName = "unitBisnis")]
        public ViewUnitBisnisModel unitBisnis { get; set; }

        [JsonProperty(PropertyName = "adk")]
        public ViewAdkModel adk { get; set; }

        [JsonProperty(PropertyName = "sbo")]
        public ViewSboModel sbo { get; set; }

        [JsonProperty(PropertyName = "durasi")]
        public int durasi { get; set; }

        [JsonProperty(PropertyName = "eta")]
        public int eta { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string status { get; set; }
    }
}