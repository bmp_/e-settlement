
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Models
{
    public class RetrieveAssignUserResult
    {
        
        [JsonProperty(PropertyName = "result")]
        public RetrieveAssignUser result { get; set; }
    }
}