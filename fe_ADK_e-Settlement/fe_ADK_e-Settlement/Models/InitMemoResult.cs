using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Models
{
    public class InitMemoResult
    {
        [JsonProperty(PropertyName = "result")]
        public InitMemo result { get; set; }
    }
}