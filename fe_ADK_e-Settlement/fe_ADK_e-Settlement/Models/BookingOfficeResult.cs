using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Models
{
    public class BookingOfficerResult
    {
        [JsonProperty(PropertyName = "result")]
        public BookingOffice result { get; set; }
    }
}
