using System.Collections.Generic;
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Models
{
    public class RetrieveAssignUser
    {
        
        [JsonProperty(PropertyName = "data")]
        public List<RetrieveAssignUserData> data { get; set; }
    }
}