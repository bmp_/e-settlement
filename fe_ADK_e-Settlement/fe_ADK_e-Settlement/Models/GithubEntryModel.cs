using System;
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Models
{
    public class GithubEntryModel
    {
        [JsonProperty(PropertyName = "userId")]
        public int userId { get; set; }

        [JsonProperty(PropertyName = "id")]
        public int id { get; set; }

        [JsonProperty(PropertyName = "title")]
        public string title { get; set; }

        [JsonProperty(PropertyName = "completed")]
        public Boolean completed { get; set; }

    }
}