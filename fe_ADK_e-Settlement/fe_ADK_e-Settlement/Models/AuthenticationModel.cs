using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Models
{
    public class AuthenticationModel
    {
        
        [JsonProperty(PropertyName = "email")]
        public string email { get; set; }

        [JsonProperty(PropertyName = "nama")]
        public string nama { get; set; }

        [JsonProperty(PropertyName = "npp")]
        public string npp { get; set; }

        [JsonProperty(PropertyName = "role")]
        public string role { get; set; }

        [JsonProperty(PropertyName = "divisi")]
        public string divisi { get; set; }
    }
}