using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Models
{
    public class CoreOFAModel
    {
        [JsonProperty(PropertyName = "code")]
        public int code { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string message { get; set; }

        [JsonProperty(PropertyName = "data")]
        public CoreOFADataModel data { get; set; }
    }
}