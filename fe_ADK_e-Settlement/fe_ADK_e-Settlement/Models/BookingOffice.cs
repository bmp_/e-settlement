﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Models
{
    public class BookingOffice
    {
        [JsonProperty(PropertyName = "data")]
        public List<BookingOfficeData> data { get; set; }

        [JsonProperty(PropertyName = "totalRows")]
        public string totalRows { get; set; }


        [JsonProperty(PropertyName = "message")]
        public string message { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string status { get; set; }
    }
}
