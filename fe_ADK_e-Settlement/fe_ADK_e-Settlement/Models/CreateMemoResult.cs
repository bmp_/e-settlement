using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Models
{
    public class CreateMemoResult
    {
        [JsonProperty(PropertyName = "result")]
        public CreateMemo result { get; set; }
    }
}