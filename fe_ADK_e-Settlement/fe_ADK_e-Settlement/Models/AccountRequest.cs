using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Models
{
    public class AccountRequest
    {
        [JsonProperty(PropertyName = "noRek")]
        public string noRek { get; set; }
    }
}