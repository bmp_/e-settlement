using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Models
{
    public class CreateMemo
    {
        
        [JsonProperty(PropertyName = "idMemo")]
        public string idMemo { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string message { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string status { get; set; }
    }
}