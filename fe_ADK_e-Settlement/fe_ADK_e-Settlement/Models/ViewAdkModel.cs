﻿using System;
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Models
{
    public class ViewAdkModel
    {
        [JsonProperty(PropertyName = "distribusi")]
        public string distribusi { get; set; }

        [JsonProperty(PropertyName = "inputer")]
        public string inputer { get; set; }

        [JsonProperty(PropertyName = "checker")]
        public string checker { get; set; }

        [JsonProperty(PropertyName = "approver")]
        public string approver { get; set; }

    }
}
