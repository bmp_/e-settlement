using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Models
{
    public class UploadFile
    {
        [JsonProperty(PropertyName = "message")]
        public string message { get; set; }
        
        [JsonProperty(PropertyName = "status")]
        public string status { get; set; }
    }
}