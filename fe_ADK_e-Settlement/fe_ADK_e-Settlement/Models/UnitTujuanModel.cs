﻿using System;
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Models
{
    public class UnitTujuanModel
    {
        [JsonProperty(PropertyName = "id")]
        public string id { get; set; }

        [JsonProperty(PropertyName = "unitTujuan")]
        public string unitTujuan { get; set; }

    }
}
