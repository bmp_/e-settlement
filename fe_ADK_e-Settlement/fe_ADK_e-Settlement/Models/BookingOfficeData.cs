using System;
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Models
{
    public class BookingOfficeData
    {
        [JsonProperty(PropertyName = "idUnit")]
        public string idUnit { get; set; }

        [JsonProperty(PropertyName = "kodeUnit")]
        public string kodeUnit { get; set; }

        [JsonProperty(PropertyName = "nama")]
        public string nama { get; set; }

        [JsonProperty(PropertyName = "tipe")]
        public string tipe { get; set; }

        [JsonProperty(PropertyName = "totalRows")]
        public string totalRows { get; set; }
    }
}
