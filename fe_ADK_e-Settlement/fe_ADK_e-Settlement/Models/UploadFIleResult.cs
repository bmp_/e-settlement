using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Models
{
    public class UploadFIleResult
    {
        [JsonProperty(PropertyName = "result")]
        public UploadFile result { get; set; }
    }
}