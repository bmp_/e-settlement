﻿using System;
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Models
{
    public class TimeProcessModel
    {
        [JsonProperty(PropertyName = "jenisTransaksi")]
        public string jenisTransaksi { get; set; }

        [JsonProperty(PropertyName = "namaProses")]
        public string namaProses { get; set; }

        [JsonProperty(PropertyName = "waktu")] 
        public int waktu { get; set; }
    }
}
