﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using fe_ADK_e_Settlement.Controllers.Api.Model;
using fe_ADK_e_Settlement.Utils;
using fe_ADK_e_Settlement.Models;
using Newtonsoft.Json;
using fe_ADK_e_Settlement.Utils.Helper;
using Microsoft.AspNetCore.WebUtilities;
using System.Collections.Generic;

namespace fe_ADK_e_Settlement.Manager
{
     public class AccessTokenManager
     {
          HttpClient client = new HttpClient();

          public static string role;
          public static string token;
          public static string expiration;

          public AccessTokenManager()
          {

          }

          public static string getAccessToken()
          {
               AccessTokenManager manager = new AccessTokenManager();
               if (token == "" || token == null)
               {
                    _ = manager.Authenticate(role);
               }

               return token;
          }

          public static bool ValidateAccessToken()
          {
               bool result = false;
               Console.WriteLine(token);
              if (token != null)
               {
                    result = true;
               }
               return result;
          }

          public static bool DeleteAccessToken()
          {
               bool result = true;
               token = null;
               return result;
          }

          public async Task<HttpResponseMessage> Authenticate(string npp)
          {
               HttpResponseMessage responseMessage = new HttpResponseMessage();
               //string url = Constant.API_USER_GET_TOKEN_URL + "?npp=" + npp;
               string url = Constant.API_ADK_GET_TOKEN_PROCESS + "?npp=" + npp;
               try
               {
                    client.BaseAddress = new Uri(url);
                    client.DefaultRequestHeaders
                         .Accept
                         .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header

                    responseMessage = await client.PostAsync(url, null).ConfigureAwait(false);

                    string responseBody = responseMessage.Content.ReadAsStringAsync().Result;
                    AuthenticationResponse authResponse = JsonConvert.DeserializeObject<AuthenticationResponse>(responseBody);

                    token = authResponse.token;
                    expiration = DateHelper.ConvertUTCDateTime(authResponse.expiration);
               }
               catch (HttpRequestException e)
               {
                    Console.WriteLine("\nException Caught!");
                    Console.WriteLine("Message :{0} ", e.Message);
               }

               return responseMessage;
          }
     }

}
