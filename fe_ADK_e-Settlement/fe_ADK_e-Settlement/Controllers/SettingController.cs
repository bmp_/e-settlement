﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using fe_ADK_e_Settlement.Controllers.Api;
using fe_ADK_e_Settlement.Controllers.Models;
using fe_ADK_e_Settlement.Controllers.Sample;
using fe_ADK_e_Settlement.Models;
using fe_ADK_e_Settlement.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using UAParser;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace fe_ADK_e_Settlement.Controllers
{

     [Route("setting")]
     public class SettingController : BaseController
     {
          // GET: /<controller>/
          public IActionResult Index()
          {
               ViewBag.JenisTransaksi = GetJenisTransaksi();
               return View();
          }

        [HttpGet("jenis-transaksi")]
        public List<JenisTransaksiData> GetJenisTransaksi()
          {
               List<JenisTransaksiData> list = new List<JenisTransaksiData>();
               if (validateSession())
               {
                    try
                    {
                         APIMasterController controller = new APIMasterController();
                         list = controller.GetJenisTransaksiAsync().Result;
                    }
                    catch (Exception e)
                    {
                         Console.WriteLine("[GetJenisTransaksi] " + e.Message);
                    }
               }
               return list;
          }


        [HttpGet("jenis-transaksi/{idTransaksi}")]
          public IActionResult GetJenisTransaksiById(string idTransaksi)
          {
               if (validateSession())
               {
                    try
                    {
                         APIMasterController controller = new APIMasterController();
                         JenisTransaksiData data = controller.GetJenisTransaksiByIdAsync(idTransaksi).Result;
                         if (data != null)
                         {
                              return Ok(buildSuccessResponse(data));
                         }
                    }
                    catch (Exception e)
                    {
                         return BadRequest(buildErrorResponse(400, e.Message));
                    }
               }
               else
               {
                    return Unauthorized(buildErrorResponse(401, "Unauthorized"));
               }
            return BadRequest(buildErrorResponse(400, "Bad Request"));
        }

          [HttpPost("jenis-transaksi")]
          public IActionResult TambahJenisTransaksi([FromBody] JenisTransaksiData request)
          {
               if (validateSession())
               {
                    try
                    {
                         APIMasterController controller = new APIMasterController();
                         bool result = controller.CreateJenisTransaksi(request).Result;
                         if (result)
                         {
                              return Ok(buildSuccessResponse());
                         }
                    }
                    catch (Exception e)
                    {
                         return BadRequest(buildErrorResponse(400, e.Message));
                    }
               }
               else
               {
                    return Unauthorized(buildErrorResponse(401, "Unauthorized"));
               }

            return BadRequest(buildErrorResponse(400, "Bad Request"));
        }

          [HttpPut("jenis-transaksi")]
          public IActionResult UpdateJenisTransaksi([FromBody] JenisTransaksiData request)
          {
               if (validateSession())
               {
                    try
                    {
                         APIMasterController controller = new APIMasterController();
                         bool result = controller.UpdateJenisTransaksi(request).Result;
                         if (result)
                         {
                              return Ok(buildSuccessResponse());
                         }
                    }
                    catch (Exception e)
                    {
                         return BadRequest(buildErrorResponse(400, e.Message));
                    }
               }
               else
               {
                    return Unauthorized(buildErrorResponse(401, "Unauthorized"));
               }
            return BadRequest(buildErrorResponse(400, "Bad Request"));
        }

          [HttpDelete("jenis-transaksi/{idTransaksi}")]
          public IActionResult DeleteJenisTransaksi(string idTransaksi)
          {
               if (validateSession())
               {
                    try
                    {
                         APIMasterController controller = new APIMasterController();
                         bool result = controller.DeleteJenisTransaksi(idTransaksi).Result;
                         if (result)
                         {
                              return Ok(buildSuccessResponse());
                         }
                    }
                    catch (Exception e)
                    {
                         return BadRequest(buildErrorResponse(400, e.Message));
                    }
               }
               else
               {
                    return Unauthorized(buildErrorResponse(401, "Unauthorized"));
               }
            return BadRequest(buildErrorResponse(400, "Bad Request"));
        }


          //MASTER TIME PROCESS

          [HttpGet("time-proses/all/{idTransaksi}")]
          public IActionResult GetTimeProcessByIdTransaksi(string idTransaksi)
          {
               if (validateSession())
               {
                    try
                    {
                         APIMasterController controller = new APIMasterController();
                         List<TimeProcessData> data = controller.GetTimeProcessByIdTransaksi(idTransaksi).Result;
                         if (data != null)
                         {
                              return Ok(buildSuccessResponse(data));
                         }
                    }
                    catch (Exception e)
                    {
                         return BadRequest(buildErrorResponse(400, e.Message));
                    }
               }
               else
               {
                    return Unauthorized(buildErrorResponse(401, "Unauthorized"));
               }
            return BadRequest(buildErrorResponse(400, "Bad Request"));
        }

          [HttpGet("time-proses/{idProcess}")]
          public IActionResult GetOneTimeProcess(string idProcess)
          {
               if (validateSession())
               {
                    try
                    {
                         APIMasterController controller = new APIMasterController();
                         TimeProcessData data = controller.GetOneTimeProcess(idProcess).Result;
                         if (data != null)
                         {
                              return Ok(buildSuccessResponse(data));
                         }
                    }
                    catch (Exception e)
                    {
                         return BadRequest(buildErrorResponse(400, e.Message));
                    }
               }
               else
               {
                    return Unauthorized(buildErrorResponse(401, "Unauthorized"));
               }
            return BadRequest(buildErrorResponse(400, "Bad Request"));
        }

          [HttpPost("time-proses")]
          public IActionResult TambahTimpeProcess([FromBody] TimeProcessRequest request)
          {
               if (validateSession())
               {
                    try
                    {
                         APIMasterController controller = new APIMasterController();
                         bool result = controller.CreateTimeProcess(request).Result;
                         if (result)
                         {
                              return Ok(buildSuccessResponse());
                         }
                    }
                    catch (Exception e)
                    {
                         return BadRequest(buildErrorResponse(400, e.Message));
                    }
               }
               else
               {
                    return Unauthorized(buildErrorResponse(401, "Unauthorized"));
               }
            return BadRequest(buildErrorResponse(400, "Bad Request"));
        }

          [HttpPut("time-proses")]
          public IActionResult UpdateTimeProcess([FromBody] TimeProcessRequest request)
          {
               if (validateSession())
               {
                    try
                    {
                         APIMasterController controller = new APIMasterController();
                         bool result = controller.UpdateTimeProcess(request).Result;
                         if (result)
                         {
                              return Ok(buildSuccessResponse());
                         }
                    }
                    catch (Exception e)
                    {
                         return BadRequest(buildErrorResponse(400, e.Message));
                    }
               }
               else
               {
                    return Unauthorized(buildErrorResponse(401, "Unauthorized"));
               }
            return BadRequest(buildErrorResponse(400, "Bad Request"));
        }

          [HttpDelete("time-proses/{idProses}")]
          public IActionResult DeleteTimeProcess(string idProses)
          {
               if (validateSession())
               {
                    try
                    {
                         APIMasterController controller = new APIMasterController();
                         bool result = controller.DeleteTimeProcess(idProses).Result;
                         if (result)
                         {
                              return Ok(buildSuccessResponse());
                         }
                    }
                    catch (Exception e)
                    {
                         return BadRequest(buildErrorResponse(400, e.Message));
                    }
               }
               else
               {
                    return Unauthorized(buildErrorResponse(401, "Unauthorized"));
               }
            return BadRequest(buildErrorResponse(400, "Bad Request"));
        }

          //MASTER LOOKUP

          [HttpPost("lookup/order")]
          public IActionResult GetMasterLookup([FromBody]Pagination pagination)
          {
               if (validateSession())
               {
                    try
                    {
                         APIMasterController controller = new APIMasterController();
                         List<LookupData> data = controller.GetAllLookups(pagination).Result;
                         if (data != null)
                         {
                              return Ok(buildSuccessResponse(data));
                         }
                    }
                    catch (Exception e)
                    {
                         return BadRequest(buildErrorResponse(400, e.Message));
                    }
               }
               else
               {
                    return Unauthorized(buildErrorResponse(401, "Unauthorized"));
               }
            return BadRequest(buildErrorResponse(400, "Bad Request"));
        }

          [HttpPost("lookup/type")]
          public IActionResult GetMasterLookupByType([FromBody]Pagination pagination)
          {
               if (validateSession())
               {
                    try
                    {
                         APIMasterController controller = new APIMasterController();
                         List<LookupData> data = controller.GetAllLookupsByType(pagination).Result;
                         if (data != null)
                         {
                              return Ok(buildSuccessResponse(data));
                         }
                    }
                    catch (Exception e)
                    {
                         return BadRequest(buildErrorResponse(400, e.Message));
                    }
               }
               else
               {
                    return Unauthorized(buildErrorResponse(401, "Unauthorized"));
               }
            return BadRequest(buildErrorResponse(400, "Bad Request"));
        }

          [HttpPost("lookup/{idLookup}")]
          public IActionResult GetOneMasterLookup(string idLookup)
          {
               if (validateSession())
               {
                    try
                    {
                         APIMasterController controller = new APIMasterController();
                         LookupData data = controller.GetLookupById(idLookup).Result;
                         if (data != null)
                         {
                              return Ok(buildSuccessResponse(data));
                         }
                    }
                    catch (Exception e)
                    {
                         return BadRequest(buildErrorResponse(400, e.Message));
                    }
               }
               else
               {
                    return Unauthorized(buildErrorResponse(401, "Unauthorized"));
               }
            return BadRequest(buildErrorResponse(400, "Bad Request"));
        }

          [HttpPost("lookup")]
          public IActionResult AddMasterLookup([FromBody] LookupRequest request)
          {
               try
               {
                    if (validateSession())
                    {
                         var context = HttpContext;

                         APIMasterController controller = new APIMasterController();
                         bool result = controller.CreateLookup(request,context).Result;
                         if (result)
                         {
                              return Ok(buildSuccessResponse());
                         }
                         else
                         {
                              return BadRequest(buildErrorResponse(400, "Bad Request"));
                         }

                    }
                    else
                    {
                         return Unauthorized(buildErrorResponse(401, "Unauthorized"));
                    }
               }
               catch (Exception e)
               {
                    return BadRequest(buildErrorResponse(400, e.Message));
               }

          }

          [HttpPut("lookup")]
          public IActionResult UpdateMasterLookup([FromBody] LookupRequest request)
          {
               if (validateSession())
               {
                    try
                    {
                         var context = HttpContext;
                         APIMasterController controller = new APIMasterController();
                         bool result = controller.UpdateLookup(request,context).Result;
                         if (result)
                         {
                              return Ok(buildSuccessResponse());
                         }
                    }
                    catch (Exception e)
                    {
                         return BadRequest(buildErrorResponse(400, e.Message));
                    }
               }
               else
               {
                    return Unauthorized(buildErrorResponse(401, "Unauthorized"));
               }
            return BadRequest(buildErrorResponse(400, "Bad Request"));
        }

          [HttpDelete("lookup/{idLookup}")]
          public IActionResult DeleteMasterLookup(string idLookup)
          {
               try
               {
                    if (validateSession())
                    {
                          var context = HttpContext;
                         APIMasterController controller = new APIMasterController();
                         bool result = controller.DeleteLookup(idLookup,context).Result;
                         if (result)
                         {
                              return Ok(buildSuccessResponse());
                         }
                         else
                         {
                              return BadRequest(buildErrorResponse(400, "Bad Request"));
                         }
                    }
                    else
                    {
                         return Unauthorized(buildErrorResponse(401, "Unauthorized"));
                    }
               }
               catch (Exception e)
               {
                    return BadRequest(buildErrorResponse(400, e.Message));
               }
          }
     }
}
