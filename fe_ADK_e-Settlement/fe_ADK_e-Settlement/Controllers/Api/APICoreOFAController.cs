using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using fe_ADK_e_Settlement.Controllers.Models;
using fe_ADK_e_Settlement.Utils;
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Controllers.Api
{
    public class APICoreOFAController : BaseApi
    {
        public async Task<CoreOFAModel> GetAccountDetails(OFACoreRequest request)
        {

            CoreOFAModel result = new CoreOFAModel();
            try
            {

                string api_url = Constant.API_CORE_OFA_URL;

                HttpClientHandler clientHandler = new HttpClientHandler();
                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

                HttpResponseMessage respose = await HttpPostAsync(api_url, request, clientHandler);

                string responseBody = respose.Content.ReadAsStringAsync().Result;
                result = JsonConvert.DeserializeObject<CoreOFAModel>(responseBody);
            }
            catch(Exception e)
            {
                Console.WriteLine("[GetAccountDetails] " + e.Message);
            }

            return result;
        }
    }
}