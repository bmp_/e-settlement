using System;
using System.Net.Http;
using System.Threading.Tasks;
using fe_ADK_e_Settlement.Controllers.Models;
using fe_ADK_e_Settlement.Manager;
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Controllers.Api
{
     public class APIAuthenticationController : BaseApi
     {
          public async Task<AuthenticationResponse> Authenticate(string npp)
          {
               try
               {
                    AuthenticationResponse authResponse = null;

                    AccessTokenManager tokenManager = new AccessTokenManager();
                    HttpResponseMessage responseMessage = await tokenManager.Authenticate(npp);

                    string responseBody = responseMessage.Content.ReadAsStringAsync().Result;
                    authResponse = JsonConvert.DeserializeObject<AuthenticationResponse>(responseBody);

                    return authResponse;
               }
               catch (Exception)
               {
                    return null;
               }

          }

     }
}