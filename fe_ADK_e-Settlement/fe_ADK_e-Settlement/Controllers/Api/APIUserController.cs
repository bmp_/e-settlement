using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using fe_ADK_e_Settlement.Controllers.Models;
using fe_ADK_e_Settlement.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace fe_ADK_e_Settlement.Controllers.Api
{
     public class APIUserController : BaseApi
     {

          public async Task<List<AdkUserData>> GetUserAdk()
          {
               List<AdkUserData> users = new List<AdkUserData>();
               string api_url = Constant.API_GET_ADK_USER_PROCESS;

               try
               {
                    HttpResponseMessage respose = await HttpApiGetAsync(api_url);
                    if (respose.IsSuccessStatusCode)
                    {

                         string responseBody = respose.Content.ReadAsStringAsync().Result;
                         JObject objResponse = JObject.Parse(responseBody);
                         JObject resultObj = (JObject)objResponse["result"];
                         JArray dataArray = (JArray)resultObj["data"];
                         users = JsonConvert.DeserializeObject<List<AdkUserData>>(dataArray.ToString());
                    }
               }
               catch (Exception e)
               {
                    Console.WriteLine("[getUserAsync]" + e.Message);
                    users.Clear();
               }
               return users;
          }
     }
}