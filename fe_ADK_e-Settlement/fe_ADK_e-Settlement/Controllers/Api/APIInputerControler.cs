using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using fe_ADK_e_Settlement.Controllers.Models;
using fe_ADK_e_Settlement.Controllers.Sample;
using fe_ADK_e_Settlement.Utils;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace fe_ADK_e_Settlement.Controllers.Api
{
    public class APIInputerControler : BaseApi
    {

        public async Task<InitMemo> initMemoAsync()
        {
            InitMemo initMemo = new InitMemo();
            string api_url = Constant.API_MEMO_RETRIEVE_ID_URL;
            try
            {
                HttpResponseMessage response = await HttpApiProtectedGetAsync(api_url);

                if (response.IsSuccessStatusCode)
                {
                    string responseBody = response.Content.ReadAsStringAsync().Result;
                    JObject objResponse = JObject.Parse(responseBody);
                    JObject resultObj = (JObject)objResponse["result"];
                    initMemo = JsonConvert.DeserializeObject<InitMemo>(resultObj.ToString());
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("[initMemoAsync] " + e.Message);
            }

            return initMemo;
        }

        public async Task<UploadFile> UploadFileAsync(string filePath, string memoId)
        {

            UploadFile result = new UploadFile();
            string api_url = Constant.API_MEMO_FILE_UPLOAD_URL + "?idMemo=" + memoId;

            try
            {
                HttpResponseMessage response = await HttpProtectedPostFormDataAsync(api_url, filePath);
                if (response.IsSuccessStatusCode)
                {
                    string responseBody = response.Content.ReadAsStringAsync().Result;
                    JObject objResponse = JObject.Parse(responseBody);
                    JObject resultObj = (JObject)objResponse["result"];
                    JObject dataObj = (JObject)resultObj["data"];
                    result = JsonConvert.DeserializeObject<UploadFile>(dataObj.ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[UploadFileAsync] " + e.Message);
            }
            return result;
        }

        public async Task<UnitTujuan> GetUnitTujuanAsync()
        {
            UnitTujuan result = new UnitTujuan();
            string api_url = Constant.API_MEMO_RETRIEVE_UNIT_TUJUAN_URL;
            try
            {
                HttpResponseMessage response = await HttpApiProtectedGetAsync(api_url);

                if (response.IsSuccessStatusCode)
                {
                    string responseBody = response.Content.ReadAsStringAsync().Result;
                    JObject objResponse = JObject.Parse(responseBody);
                    JObject resultObj = (JObject)objResponse["result"];
                    JObject dataObj = (JObject)resultObj["data"];
                    result = JsonConvert.DeserializeObject<UnitTujuan>(dataObj.ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[initMemoAsync] " + e.Message);
            }

            return result;
        }

        public async Task<List<BookingOfficeData>> GetBookingOfficesAsync(Pagination request)
        {
            List<BookingOfficeData> result = new List<BookingOfficeData>();

            string api_url = Constant.API_MEMO_RETRIEVE_BOOKING_OFFICE_URL
            + "?OrderBy=" + request.OrderBy + "&SortBy=" + request.SortBy + "&PageNumber=" + request.PageNumber + "&PageSize=" + request.PageSize;

            try
            {
                HttpResponseMessage response = await HttpApiProtectedGetAsync(api_url);
                if (response.IsSuccessStatusCode)
                {
                    string responseBody = response.Content.ReadAsStringAsync().Result;
                    JObject objResponse = JObject.Parse(responseBody);
                    JObject resultObj = (JObject)objResponse["result"];
                    JArray dataObj = (JArray)resultObj["data"];
                    result = JsonConvert.DeserializeObject<List<BookingOfficeData>>(dataObj.ToString());
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("[GetBookingOfficesAsync] " + e.Message);
            }
            return result;
        }

        //GetUnitTujuanDebiturAsync
        public async Task<List<UnitTujuanDebitur>> GetUnitTujuanDebiturAsync(Pagination request)
        {
            List<UnitTujuanDebitur> result = new List<UnitTujuanDebitur>();

            string api_url = Constant.API_MEMO_RETRIEVE_UNIT_TUJUAN_DEBITUR_URL
            + "?OrderBy=" + request.OrderBy + "&SortBy=" + request.SortBy + "&PageNumber=" + request.PageNumber + "&PageSize=" + request.PageSize;

            try
            {
                HttpResponseMessage response = await HttpApiProtectedGetAsync(api_url);
                if (response.IsSuccessStatusCode)
                {
                    string responseBody = response.Content.ReadAsStringAsync().Result;
                    JObject objResponse = JObject.Parse(responseBody);
                    JObject resultObj = (JObject)objResponse["result"];
                    JArray dataObj = (JArray)resultObj["data"];
                    result = JsonConvert.DeserializeObject<List<UnitTujuanDebitur>>(dataObj.ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[GetBookingOfficesAsync] " + e.Message);
            }
            return result;
        }

        public async Task<List<RetrieveAssignUserData>> GetCheckerList()
        {

            List<RetrieveAssignUserData> assignUserList = new List<RetrieveAssignUserData>();
            string api_url = Constant.API_MEMO_RETRIEVE_CHECKER_URL;

            try
            {
                HttpResponseMessage respose = await HttpApiProtectedGetAsync(api_url);
                if (respose.IsSuccessStatusCode)
                {
                    string responseBody = respose.Content.ReadAsStringAsync().Result;
                    JObject objResponse = JObject.Parse(responseBody);
                    JObject resultObj = (JObject)objResponse["result"];
                    JArray dataArray = (JArray)resultObj["data"];
                    assignUserList = JsonConvert.DeserializeObject<List<RetrieveAssignUserData>>(dataArray.ToString());
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[GetCheckerList] " + e.Message);
            }

            return assignUserList;
        }

        public async Task<List<RetrieveAssignUserData>> GetApproverList()
        {

            List<RetrieveAssignUserData> assignUserList = new List<RetrieveAssignUserData>();
            string api_url = Constant.API_MEMO_RETRIEVE_APPROVER_URL;

            try
            {
                HttpResponseMessage respose = await HttpApiProtectedGetAsync(api_url);
                if (respose.IsSuccessStatusCode)
                {
                    string responseBody = respose.Content.ReadAsStringAsync().Result;
                    JObject objResponse = JObject.Parse(responseBody);
                    JObject resultObj = (JObject)objResponse["result"];
                    JArray dataArray = (JArray)resultObj["data"];
                    assignUserList = JsonConvert.DeserializeObject<List<RetrieveAssignUserData>>(dataArray.ToString());
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[GetApproverList] " + e.Message);
            }

            return assignUserList;
        }

        public async Task<bool> CreateMemoAsync(UnitBisnisRequest request)
        {
            bool result = false;

            string api_url = Constant.API_MEMO_CREATE_URL;

            HttpResponseMessage response = await HttpApiProtectedPostAsync(api_url, request);

            if (response.IsSuccessStatusCode)
            {
                result = true;
            }
            return result;
        }


    }
}