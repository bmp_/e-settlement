﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using fe_ADK_e_Settlement.Controllers.Models;
using fe_ADK_e_Settlement.Manager;
using fe_ADK_e_Settlement.Models;
using fe_ADK_e_Settlement.Utils;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using UAParser;

namespace fe_ADK_e_Settlement.Controllers.Api
{
     public class BaseApi
     {
          HttpClient client = new HttpClient();

          public BaseApi()
          {
          }
          protected async Task<HttpResponseMessage> HttpApiGetAsync(string url)
          {
               HttpResponseMessage responseMessage = new HttpResponseMessage();
               try
               {
                    client = new HttpClient();
                    client.BaseAddress = new Uri(url);
                    client.DefaultRequestHeaders
                          .Accept
                          .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header

                    responseMessage = await client.GetAsync(url).ConfigureAwait(false);
               }
               catch (HttpRequestException e)
               {
                    Console.WriteLine("\nException Caught!");
                    Console.WriteLine("Message :{0} ", e.Message);
               }

               return responseMessage;
          }

          protected async Task<HttpResponseMessage> HttpApiProtectedGetAsync(string url)
          {
               HttpResponseMessage responseMessage = new HttpResponseMessage();
               try
               {
                    client = new HttpClient();
                    client.BaseAddress = new Uri(url);
                    client.DefaultRequestHeaders
                          .Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header

                    client.DefaultRequestHeaders.Authorization =
                              new AuthenticationHeaderValue("Bearer", AccessTokenManager.getAccessToken());

                    responseMessage = await client.GetAsync(url).ConfigureAwait(false);
               }
               catch (HttpRequestException e)
               {
                    Console.WriteLine("\nException Caught!");
                    Console.WriteLine("Message :{0} ", e.Message);
               }

               return responseMessage;
          }

          protected async Task<HttpResponseMessage> HttpApiProtectedPostAsync(string url, Object request)
          {
               HttpResponseMessage responseMessage = new HttpResponseMessage();

               try
               {
                    string str = JsonConvert.SerializeObject(request, Newtonsoft.Json.Formatting.None,
                            new JsonSerializerSettings
                            {
                                 NullValueHandling = NullValueHandling.Ignore
                            });

                    client = new HttpClient();
                    client.BaseAddress = new Uri(url);
                    client.DefaultRequestHeaders
                          .Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header

                    client.DefaultRequestHeaders.Authorization =
                              new AuthenticationHeaderValue("Bearer", AccessTokenManager.getAccessToken());

                    StringContent content = new StringContent(str, Encoding.UTF8, "application/json");

                    responseMessage = await client.PostAsync(url, content).ConfigureAwait(false);
               }
               catch (HttpRequestException e)
               {
                    Console.WriteLine("\nException Caught!");
                    Console.WriteLine("Message :{0} ", e.Message);
               }

               return responseMessage;
          }

          protected async Task<HttpResponseMessage> HttpApiProtectedPostAsync(string url, Object request, HttpContext context)
          {
               HttpResponseMessage responseMessage = new HttpResponseMessage();

               try
               {

                    string userAgent = context.Request.Headers["User-Agent"];
                    var uaParser = Parser.GetDefault();
                    ClientInfo c = uaParser.Parse(userAgent);

                    string str = JsonConvert.SerializeObject(request, Newtonsoft.Json.Formatting.None,
                            new JsonSerializerSettings
                            {
                                 NullValueHandling = NullValueHandling.Ignore
                            });

                    client = new HttpClient();
                    client.DefaultRequestHeaders.Add("User-Agent", userAgent);
                    client.BaseAddress = new Uri(url);
                    client.DefaultRequestHeaders
                          .Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header

                    client.DefaultRequestHeaders.Authorization =
                              new AuthenticationHeaderValue("Bearer", AccessTokenManager.getAccessToken());

                    StringContent content = new StringContent(str, Encoding.UTF8, "application/json");

                    responseMessage = await client.PostAsync(url, content).ConfigureAwait(false);
               }
               catch (HttpRequestException e)
               {
                    Console.WriteLine("\nException Caught!");
                    Console.WriteLine("Message :{0} ", e.Message);
               }

               return responseMessage;
          }

          protected async Task<HttpResponseMessage> HttpApiProtectedPostNoBodyAsync(string url)
          {
               HttpResponseMessage responseMessage = new HttpResponseMessage();

               try
               {
                    client = new HttpClient();
                    client.BaseAddress = new Uri(url);

                    client.DefaultRequestHeaders.Authorization =
                              new AuthenticationHeaderValue("Bearer", AccessTokenManager.getAccessToken());

                    responseMessage = await client.PostAsync(url, null).ConfigureAwait(false);
               }
               catch (HttpRequestException e)
               {
                    Console.WriteLine("\nException Caught!");
                    Console.WriteLine("Message :{0} ", e.Message);
               }

               return responseMessage;
          }

          protected async Task<HttpResponseMessage> HttpPostAsync(string url, Object request, HttpClientHandler clientHandler)
          {
               HttpResponseMessage responseMessage = new HttpResponseMessage();

               try
               {

                string str = JsonConvert.SerializeObject(request);

                    client = new HttpClient(clientHandler);

                    client.BaseAddress = new Uri(url);
                    client.DefaultRequestHeaders.Add("ApiKey", "BNIPANCENOYE!");
                    client.DefaultRequestHeaders
                         .Accept
                         .Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    StringContent content = new StringContent(str, Encoding.UTF8, "application/json");

                    responseMessage = await client.PostAsync(url, content).ConfigureAwait(false);
               }
               catch (HttpRequestException e)
               {
                    Console.WriteLine("\nException Caught!");
                    Console.WriteLine("Message :{0} ", e.Message);
               }

               return responseMessage;
          }

          protected async Task<HttpResponseMessage> HttpProtectedPostFormDataAsync(string url, string filePath)
          {
               HttpResponseMessage responseMessage = new HttpResponseMessage();

               try
               {
                    client = new HttpClient();
                    client.BaseAddress = new Uri(url);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AccessTokenManager.getAccessToken());
                    using var form = new MultipartFormDataContent();
                    using var fileContent = new ByteArrayContent(await File.ReadAllBytesAsync(filePath));
                    fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("multipart/form-data");
                    form.Add(fileContent, "file", Path.GetFileName(filePath));

                    responseMessage = await client.PostAsync(url, form).ConfigureAwait(false);

               }
               catch (HttpRequestException e)
               {
                    Console.WriteLine("\nException Caught!");
                    Console.WriteLine("Message :{0} ", e.Message);
               }

               return responseMessage;
          }

          protected async Task<HttpResponseMessage> HttpApiProtectedPutAsync(string url, Object request)
          {
               HttpResponseMessage responseMessage = new HttpResponseMessage();

               try
               {
                    string str = JsonConvert.SerializeObject(request, Newtonsoft.Json.Formatting.None,
                            new JsonSerializerSettings
                            {
                                 NullValueHandling = NullValueHandling.Ignore
                            });

                    client = new HttpClient();
                    client.BaseAddress = new Uri(url);
                    client.DefaultRequestHeaders
                          .Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header

                    client.DefaultRequestHeaders.Authorization =
                              new AuthenticationHeaderValue("Bearer", AccessTokenManager.getAccessToken());

                    StringContent content = new StringContent(str, Encoding.UTF8, "application/json");

                    responseMessage = await client.PutAsync(url, content).ConfigureAwait(false);
               }
               catch (HttpRequestException e)
               {
                    Console.WriteLine("\nException Caught!");
                    Console.WriteLine("Message :{0} ", e.Message);
               }

               return responseMessage;
          }

          protected async Task<HttpResponseMessage> HttpApiProtectedDeleteAsync(string url)
          {
               HttpResponseMessage responseMessage = new HttpResponseMessage();

               try
               {
                    client = new HttpClient();
                    client.BaseAddress = new Uri(url);
                    client.DefaultRequestHeaders
                          .Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header

                    client.DefaultRequestHeaders.Authorization =
                              new AuthenticationHeaderValue("Bearer", AccessTokenManager.getAccessToken());

                    responseMessage = await client.DeleteAsync(url).ConfigureAwait(false);
               }
               catch (HttpRequestException e)
               {
                    Console.WriteLine("\nException Caught!");
                    Console.WriteLine("Message :{0} ", e.Message);
               }

               return responseMessage;
          }

          protected ServiceResponse buildSuccessResponse()
          {
               ServiceResponse response = new ServiceResponse();
               response.status = 200;
               response.message = "Success";

               return response;
          }

          protected ServiceResponse buildSuccessResponse(string message)
          {
               ServiceResponse response = new ServiceResponse();
               response.status = 200;
               response.message = message;

               return response;
          }

          protected ServiceResponse buildSuccessResponse(Object obj)
          {
               ServiceResponse response = new ServiceResponse();
               response.status = 200;
               response.message = "Success";
               response.data = obj;

               return response;
          }
          protected ServiceResponse buildErrorResponse(int status, string message)
          {
               ServiceResponse response = new ServiceResponse();
               response.status = status;
               response.message = message;

               return response;
          }


     }
}
