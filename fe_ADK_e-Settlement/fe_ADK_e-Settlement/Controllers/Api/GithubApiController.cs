using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using fe_ADK_e_Settlement.Controllers.Api.Model;
using fe_ADK_e_Settlement.Models;
using fe_ADK_e_Settlement.Utils;
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Controllers.Api
{
    public class GithubApiController : BaseApi
    {
        public GithubApiController()
        {

        }

        public async Task<List<GithubEntryModel>> GetGithubEntriesAsync()
        {

            List<GithubEntryModel> result = new List<GithubEntryModel>();

            string api_url = Constant.GITHUB_ENTRY;

            HttpResponseMessage respose = await HttpApiGetAsync("https://jsonplaceholder.typicode.com/todos");

            if(respose.IsSuccessStatusCode)
            {
                string responseBody = respose.Content.ReadAsStringAsync().Result;
                result = JsonConvert.DeserializeObject<List<GithubEntryModel>>(responseBody);
            }
            return result;
        }

        private async Task<HttpResponseMessage> HttpGetAsync(string url)
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage responseMessage = new HttpResponseMessage();

            try
            {
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders
                      .Accept
                      .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header

                responseMessage = await client.GetAsync(url).ConfigureAwait(false);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }

            return responseMessage;
        }
    }
}