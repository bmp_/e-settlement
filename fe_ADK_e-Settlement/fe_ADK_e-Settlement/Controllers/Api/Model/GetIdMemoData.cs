
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Controllers.Api.Model
{
    public class GetIdMemoData
    {
        [JsonProperty(PropertyName = "npp")]
        public string npp { get; set; }

        [JsonProperty(PropertyName = "nama")]
        public string nama { get; set; }

        [JsonProperty(PropertyName = "noTelp")]
        public string noTelp { get; set; }

        [JsonProperty(PropertyName = "noHP")]
        public string noHP { get; set; }
    }
}



