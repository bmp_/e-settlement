using System.Collections.Generic;
using fe_ADK_e_Settlement.Models;
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Controllers.Api.Model
{
    public class EntryResponse
    {
        [JsonProperty(PropertyName = "entries")]
        public List<GithubEntryModel> GithubEntry { get; set; }
    }
}