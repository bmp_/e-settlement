using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Controllers.Api.Model
{
    public class AuthenticationResponse
    {
       
        [JsonProperty(PropertyName = "token")]
        public string token { get; set; }

        [JsonProperty(PropertyName = "expiration")]
        public string expiration     { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string message { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string status { get; set; }

        [JsonProperty(PropertyName = "eSettRole")]
        public string eSettRole { get; set; }
    }
}