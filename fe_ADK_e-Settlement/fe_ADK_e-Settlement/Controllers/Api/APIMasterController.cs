using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using fe_ADK_e_Settlement.Controllers.Models;
using fe_ADK_e_Settlement.Models;
using fe_ADK_e_Settlement.Utils;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace fe_ADK_e_Settlement.Controllers.Api
{
     public class APIMasterController : BaseApi
     {

          //API MASTER JENIS TRANSAKSI
          public async Task<List<JenisTransaksiData>> GetJenisTransaksiAsync()
          {
               List<JenisTransaksiData> result = new List<JenisTransaksiData>();
               string api_url = Constant.API_MASTER_RETRIEVE_JENIS_TRANSAKSI;
               try
               {
                    HttpResponseMessage respose = await HttpApiProtectedGetAsync(api_url);
                    if (respose.IsSuccessStatusCode)
                    {
                         string responseBody = respose.Content.ReadAsStringAsync().Result;
                         JObject objResponse = JObject.Parse(responseBody);
                         JObject resultObj = (JObject)objResponse["result"];
                         JArray dataArray = (JArray)resultObj["data"];
                         result = JsonConvert.DeserializeObject<List<JenisTransaksiData>>(dataArray.ToString());
                    }
                    else
                    {
                         return null;
                    }
               }
               catch (Exception e)
               {
                    Console.WriteLine("[GetJenisTransaksi] " + e.Message);
               }
               return result;
          }

          public async Task<JenisTransaksiData> GetJenisTransaksiByIdAsync(string idTransaksi)
          {
               JenisTransaksiData result = new JenisTransaksiData();
               string api_url = Constant.API_MASTER_RETRIEVE_ONE_JENIS_TRANSAKSI + "/" + idTransaksi;
               try
               {
                    HttpResponseMessage respose = await HttpApiProtectedGetAsync(api_url);
                    if (respose.IsSuccessStatusCode)
                    {
                         string responseBody = respose.Content.ReadAsStringAsync().Result;
                         JObject objResponse = JObject.Parse(responseBody);
                         JObject resultObj = (JObject)objResponse["result"];
                         JObject dataObj = (JObject)resultObj["data"];
                         result = JsonConvert.DeserializeObject<JenisTransaksiData>(dataObj.ToString());
                    }
                    else
                    {
                         return null;
                    }
               }
               catch (Exception e)
               {
                    Console.WriteLine("[GetJenisTransaksiById] " + e.Message);
               }
               return result;
          }

          public async Task<bool> CreateJenisTransaksi(JenisTransaksiData request)
          {
               bool result = false;
               string api_url = Constant.API_MASTER_CREATE_JENIS_TRANSAKSI;
               try
               {
                    HttpResponseMessage respose = await HttpApiProtectedPostAsync(api_url, request);
                    if (respose.IsSuccessStatusCode)
                    {
                         result = true;
                    }
               }
               catch (Exception e)
               {
                    Console.WriteLine("[CreateJenisTransaksi] " + e.Message);
               }
               return result;
          }

          public async Task<bool> UpdateJenisTransaksi(JenisTransaksiData request)
          {
               bool result = false;
               string api_url = Constant.API_MASTER_UPDATE_JENIS_TRANSAKSI;
               try
               {
                    HttpResponseMessage respose = await HttpApiProtectedPutAsync(api_url, request);
                    if (respose.IsSuccessStatusCode)
                    {
                         result = true;
                    }
               }
               catch (Exception e)
               {
                    Console.WriteLine("[UpdateJenisTransaksi] " + e.Message);
               }
               return result;
          }

          public async Task<bool> DeleteJenisTransaksi(string idTransaksi)
          {
               bool result = false;
               string api_url = Constant.API_MASTER_DELETE_JENIS_TRANSAKSI + "/" + idTransaksi;
               try
               {
                    HttpResponseMessage respose = await HttpApiProtectedDeleteAsync(api_url);
                    if (respose.IsSuccessStatusCode)
                    {
                         result = true;
                    }
               }
               catch (Exception e)
               {
                    Console.WriteLine("[DeleteJenisTransaksi] " + e.Message);
               }
               return result;
          }


          // MASTER LOOKUP
          public async Task<List<LookupData>> GetAllLookups(Pagination pagination)
          {
               List<LookupData> result = new List<LookupData>();
               string api_url = Constant.API_MASTER_RETRIEVE_ALL_LOOKUP +
               "?OrderBy="+pagination.OrderBy+"&SortBy="+pagination.SortBy+"&PageNumber="+pagination.PageNumber+"&PageSize="+pagination.PageSize;
               try
               {
                    HttpResponseMessage respose = await HttpApiProtectedGetAsync(api_url);
                    if (respose.IsSuccessStatusCode)
                    {
                         string responseBody = respose.Content.ReadAsStringAsync().Result;
                         JObject objResponse = JObject.Parse(responseBody);
                         JObject resultObj = (JObject)objResponse["result"];
                         JArray dataArray = (JArray)resultObj["data"];
                         result = JsonConvert.DeserializeObject<List<LookupData>>(dataArray.ToString());
                    }
                    else
                    {
                         return null;
                    }
               }
               catch (Exception e)
               {
                    Console.WriteLine("[GetAllLookups] " + e.Message);
               }
               return result;
          }

          public async Task<List<LookupData>> GetAllLookupsByType(Pagination pagination)
          {
               List<LookupData> result = new List<LookupData>();
               string api_url = Constant.API_MASTER_RETRIEVE_LOOKUP_BY_TYPE +
               "?tipe="+pagination.tipe+"&OrderBy="+pagination.OrderBy+"&SortBy="+pagination.SortBy+"&PageNumber="+pagination.PageNumber+"&PageSize="+pagination.PageSize;
               try
               {
                    HttpResponseMessage respose = await HttpApiProtectedGetAsync(api_url);
                    if (respose.IsSuccessStatusCode)
                    {
                         string responseBody = respose.Content.ReadAsStringAsync().Result;
                         JObject objResponse = JObject.Parse(responseBody);
                         JObject resultObj = (JObject)objResponse["result"];
                         JArray dataArray = (JArray)resultObj["data"];
                         result = JsonConvert.DeserializeObject<List<LookupData>>(dataArray.ToString());
                    }
                    else
                    {
                         return null;
                    }
               }
               catch (Exception e)
               {
                    Console.WriteLine("[GetAllLookupsByType] " + e.Message);
               }
               return result;
          }

          public async Task<LookupData> GetLookupById(string idLookup)
          {
               LookupData result = new LookupData();
               string api_url = Constant.API_MASTER_GET_ONE_LOOKUP + "/" + idLookup;
               try
               {
                    HttpResponseMessage respose = await HttpApiProtectedGetAsync(api_url);
                    if (respose.IsSuccessStatusCode)
                    {
                         string responseBody = respose.Content.ReadAsStringAsync().Result;
                         JObject objResponse = JObject.Parse(responseBody);
                         JObject resultObj = (JObject)objResponse["result"];
                         JObject dataObj = (JObject)resultObj["data"];
                         result = JsonConvert.DeserializeObject<LookupData>(dataObj.ToString());
                    }
                    else
                    {
                         return null;
                    }
               }
               catch (Exception e)
               {
                    Console.WriteLine("[GetLookupById] " + e.Message);
               }
               return result;
          }

          public async Task<bool> CreateLookup(LookupRequest request, HttpContext context)
          {
               bool result = false;
               string api_url = Constant.API_MASTER_CREATE_LOOKUP;
               try
               {
                    HttpResponseMessage respose = await HttpApiProtectedPostAsync(api_url, request, context);

                    if (respose.IsSuccessStatusCode)
                    {
                         result = true;
                    }
               }
               catch (Exception e)
               {
                    Console.WriteLine("[CreateLookup] " + e.Message);
               }
               return result;
          }

          public async Task<bool> UpdateLookup(LookupRequest request, HttpContext context)
          {
               bool result = false;
               string api_url = Constant.API_MASTER_UPDATE_LOOKUP;
               try
               {
                    HttpResponseMessage respose = await HttpApiProtectedPostAsync(api_url, request, context);
                    if (respose.IsSuccessStatusCode)
                    {
                         result = true;
                    }
               }
               catch (Exception e)
               {
                    Console.WriteLine("[UpdateLookup] " + e.Message);
               }
               return result;
          }

          public async Task<bool> DeleteLookup(string idLookup, HttpContext context)
          {
               bool result = false;
               string api_url = Constant.API_MASTER_DELETE_LOOKUP + "/" + idLookup;
               try
               {
                    HttpResponseMessage respose = await HttpApiProtectedPostAsync(api_url, null, context);
                    if (respose.IsSuccessStatusCode)
                    {
                         result = true;
                    }
               }
               catch (Exception e)
               {
                    Console.WriteLine("[DeleteLookup] " + e.Message);
               }
               return result;
          }

          // MASTER TIMEPROCESS
          public async Task<List<TimeProcessData>> GetTimeProcessByIdTransaksi(string idTransaksi)
          {
               List<TimeProcessData> result = new List<TimeProcessData>();
               string api_url = Constant.API_MASTER_RETRIEVE_ALL_TIME_PROCESS +"?IdJenisTransaksi="+ idTransaksi;
               try
               {
                    HttpResponseMessage respose = await HttpApiProtectedGetAsync(api_url);
                    if (respose.IsSuccessStatusCode)
                    {
                         string responseBody = respose.Content.ReadAsStringAsync().Result;
                         JObject objResponse = JObject.Parse(responseBody);
                         JObject resultObj = (JObject)objResponse["result"];
                         JArray dataArray = (JArray)resultObj["data"];
                         result = JsonConvert.DeserializeObject<List<TimeProcessData>>(dataArray.ToString());
                    }
                    else
                    {
                         return null;
                    }
               }
               catch (Exception e)
               {
                    Console.WriteLine("[GetTimeProcessByIdTransaksi] " + e.Message);
               }
               return result;
          }

          public async Task<TimeProcessData> GetOneTimeProcess(string idProses)
          {
               TimeProcessData result = new TimeProcessData();
               string api_url = Constant.API_MASTER_RETRIEVE_ONE_TIME_PROCESS + "/" + idProses;
               try
               {
                    HttpResponseMessage respose = await HttpApiProtectedGetAsync(api_url);
                    if (respose.IsSuccessStatusCode)
                    {
                         string responseBody = respose.Content.ReadAsStringAsync().Result;
                         JObject objResponse = JObject.Parse(responseBody);
                         JObject resultObj = (JObject)objResponse["result"];
                         JObject dataObj = (JObject)resultObj["data"];
                         result = JsonConvert.DeserializeObject<TimeProcessData>(dataObj.ToString());
                    }
                    else
                    {
                         return null;
                    }
               }
               catch (Exception e)
               {
                    Console.WriteLine("[GetOneTimeProcess] " + e.Message);
               }
               return result;
          }

          public async Task<bool> CreateTimeProcess(TimeProcessRequest request)
          {
               bool result = false;
               string api_url = Constant.API_MASTER_CREATE_TIME_PROCESS;
               try
               {
                    HttpResponseMessage respose = await HttpApiProtectedPostAsync(api_url, request);

                    if (respose.IsSuccessStatusCode)
                    {
                         result = true;
                    }
               }
               catch (Exception e)
               {
                    Console.WriteLine("[CreateTimeProcess] " + e.Message);
               }
               return result;
          }

          public async Task<bool> UpdateTimeProcess(TimeProcessRequest request)
          {
               bool result = false;
               string api_url = Constant.API_MASTER_UPDATE_TIME_PROCESS;
               try
               {
                    HttpResponseMessage respose = await HttpApiProtectedPutAsync(api_url, request);

                    if (respose.IsSuccessStatusCode)
                    {
                         result = true;
                    }
               }
               catch (Exception e)
               {
                    Console.WriteLine("[UpdateTimeProcess] " + e.Message);
               }
               return result;
          }

          public async Task<bool> DeleteTimeProcess(string idProcess)
          {
               bool result = false;
               string api_url = Constant.API_MASTER_DELETE_TIME_PROCESS + "/" + idProcess;
               try
               {
                    HttpResponseMessage respose = await HttpApiProtectedDeleteAsync(api_url);
                    if (respose.IsSuccessStatusCode)
                    {
                         result = true;
                    }
               }
               catch (Exception e)
               {
                    Console.WriteLine("[DeleteTimeProcess] " + e.Message);
               }
               return result;
          }

     }
}




