﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using fe_ADK_e_Settlement.Models;
using fe_ADK_e_Settlement.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace fe_ADK_e_Settlement.Controllers.Api
{
    public class APITimeProcessController : BaseApi
    {
        public APITimeProcessController()
        {
        }

        // public async Task<IList<JenisTransaksiModel>> GetJenisTransaksiAsync()
        // {
        //     IList<JenisTransaksiModel> result = new List<JenisTransaksiModel>();

        //     string api_url = Constant.API_GET_JENIS_TRANSAKSI_URL;

        //     HttpResponseMessage respose = await HttpApiProtectedGetAsync(api_url);

        //     if (respose.IsSuccessStatusCode)
        //     {
        //         string responseBody = respose.Content.ReadAsStringAsync().Result;
        //         JObject obj = JObject.Parse(responseBody);
        //         JArray resultArray = (JArray)obj["data"];
        //         result = JsonConvert.DeserializeObject<IList<JenisTransaksiModel>>(resultArray.ToString());
        //     }

        //     return result;
        // }

        public async Task<IList<TimeProcessModel>> getTimeProcessAsyc()
        {

            IList<TimeProcessModel> result = new List<TimeProcessModel>();

            string api_url = Constant.API_GET_TIME_PROCESS_URL;

            HttpResponseMessage respose = await HttpApiProtectedGetAsync(api_url);

            if (respose.IsSuccessStatusCode)
            {
                string responseBody = respose.Content.ReadAsStringAsync().Result;
                JObject obj = JObject.Parse(responseBody);
                JArray resultArray = (JArray)obj["data"];
                result = JsonConvert.DeserializeObject<IList<TimeProcessModel>>(resultArray.ToString());
            }
            return result;
        }
    }
}
