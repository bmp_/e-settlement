using System.Collections.Generic;
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Controllers.Models
{
     public class Pagination
     {

          [JsonProperty(PropertyName = "OrderBy")]
          public string OrderBy { get; set; }

          [JsonProperty(PropertyName = "SortBy")]
          public string SortBy { get; set; }

          [JsonProperty(PropertyName = "PageNumber")]
          public string PageNumber { get; set; }

          [JsonProperty(PropertyName = "PageSize")]
          public string PageSize { get; set; }

          [JsonProperty(PropertyName = "idParent")]
          public string idParent { get; set; }
          
          [JsonProperty(PropertyName = "tipe")]
          public string tipe { get; set; }

     }
}