using System;
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Controllers.Models
{
    public class BookingOfficeData
    {
        [JsonProperty(PropertyName = "id")]
        public int id { get; set; }

        [JsonProperty(PropertyName = "nama")]
        public string nama { get; set; }

        [JsonProperty(PropertyName = "totalRows")]
        public int totalRows { get; set; }
    }
}
