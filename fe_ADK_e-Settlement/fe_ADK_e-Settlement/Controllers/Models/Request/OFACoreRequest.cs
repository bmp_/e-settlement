using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Controllers.Models
{
    public class OFACoreRequest
    {
        [JsonProperty(PropertyName = "noRek")]
        public string noRek { get; set; }
    }
}