using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Controllers.Models
{
    public class UnitBisnisWaktuProcess
    {
        [JsonProperty(PropertyName = "namaProses")]
        public string namaProses { get; set; }

        [JsonProperty(PropertyName = "idJenisTransaksi")]
        public string idJenisTransaksi { get; set; }

        [JsonProperty(PropertyName = "jenisTransaksi")]
        public string jenisTransaksi { get; set; }

        [JsonProperty(PropertyName = "waktu")]
        public long waktu { get; set; }

        [JsonProperty(PropertyName = "jumlahTransaksi")]
        public long jumlahTransaksi { get; set; }

        [JsonProperty(PropertyName = "totalWaktu")]
        public long totalWaktu { get; set; }
    }
}