using System.Collections.Generic;
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Controllers.Models
{
    public class UnitBisnisRequest
    {
        [JsonProperty(PropertyName = "idMemo")]
        public string idMemo { get; set; }

        [JsonProperty(PropertyName = "generatedId")]
        public long generatedId { get; set; }

        [JsonProperty(PropertyName = "unitAsal")]
        public string unitAsal { get; set; }

        [JsonProperty(PropertyName = "unitTujuanParent")]
        public string unitTujuanParent { get; set; }

        [JsonProperty(PropertyName = "unitTujuanChild")]
        public string unitTujuanChild { get; set; }

        [JsonProperty(PropertyName = "noMemo")]
        public string noMemo { get; set; }

        [JsonProperty(PropertyName = "tglMemo")]
        public string tglMemo { get; set; }

        [JsonProperty(PropertyName = "komentar")]
        public string komentar { get; set; }

        [JsonProperty(PropertyName = "jenisTransaksiDebitur")]
        public string jenisTransaksiDebitur { get; set; }

        [JsonProperty(PropertyName = "cabangBookingOffice")]
        public string cabangBookingOffice { get; set; }

        [JsonProperty(PropertyName = "noRekening")]
        public string noRekening { get; set; }

        [JsonProperty(PropertyName = "namaDebitur")]
        public string namaDebitur { get; set; }

        [JsonProperty(PropertyName = "cif")]
        public string cif { get; set; }

        [JsonProperty(PropertyName = "currency")]
        public string currency { get; set; }

        [JsonProperty(PropertyName = "nominal")]
        public long nominal { get; set; }

        [JsonProperty(PropertyName = "unitTujuanDebitur")]
        public string unitTujuanDebitur { get; set; }

        [JsonProperty(PropertyName = "npp")]
        public string npp { get; set; }
        
        [JsonProperty(PropertyName = "namaRo")]
        public string namaRo { get; set; }

        [JsonProperty(PropertyName = "noTelp")]
        public string noTelp { get; set; }

        [JsonProperty(PropertyName = "noHP")]
        public string noHP { get; set; }
        
        [JsonProperty(PropertyName = "flagStatus")]
        public string flagStatus { get; set; }

        [JsonProperty(PropertyName = "nppChecker")]
        public string nppChecker { get; set; }

        [JsonProperty(PropertyName = "nppApprover")]
        public string nppApprover { get; set; }

        

        [JsonProperty(PropertyName = "waktuProses")]
        public List<UnitBisnisWaktuProcess> waktuProses { get; set; }

    }
}