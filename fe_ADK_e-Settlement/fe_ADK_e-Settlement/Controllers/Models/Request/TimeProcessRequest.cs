using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Controllers.Models
{
     public class TimeProcessRequest
     {
          [JsonProperty(PropertyName = "idJenisTransaksi")]
          public string idJenisTransaksi { get; set; }

          [JsonProperty(PropertyName = "idProses")]
          public string idProses { get; set; }

          [JsonProperty(PropertyName = "namaProses")]
          public string namaProses { get; set; }

          [JsonProperty(PropertyName = "waktu")]
          public int waktu { get; set; }
     }
}