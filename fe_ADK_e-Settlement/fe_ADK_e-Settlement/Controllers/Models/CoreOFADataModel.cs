using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Controllers.Models
{
    public class CoreOFADataModel
    {
        [JsonProperty(PropertyName = "nama")]
        public string nama { get; set; }

        [JsonProperty(PropertyName = "cif")]
        public string cif { get; set; }
    }
}