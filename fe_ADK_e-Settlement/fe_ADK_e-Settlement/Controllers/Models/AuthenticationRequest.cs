using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Controllers.Models
{
    public class AuthenticationRequest
    {
       
        [JsonProperty(PropertyName = "npp")]
        public string npp { get; set; }
    }
}