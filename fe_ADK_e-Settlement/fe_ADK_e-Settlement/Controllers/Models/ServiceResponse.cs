using System;
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Controllers.Models
{
    public class ServiceResponse
    {
        [JsonProperty(PropertyName = "status")]
        public int status { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string message { get; set; }

        [JsonProperty(PropertyName = "data")]
        public Object data { get; set; }
    }
}