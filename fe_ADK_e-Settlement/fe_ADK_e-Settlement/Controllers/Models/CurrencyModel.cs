﻿using System;
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Controllers.Models
{
    public class CurrencyModel
    {
        [JsonProperty(PropertyName = "id")]
        public int id { get; set; }

        [JsonProperty(PropertyName = "currency")]
        public string currency { get; set; }
    }
}
