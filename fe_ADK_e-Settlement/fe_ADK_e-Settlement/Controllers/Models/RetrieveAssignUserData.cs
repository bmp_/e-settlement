﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fe_ADK_e_Settlement.Controllers.Models
{
    public class RetrieveAssignUserData
    {
        [JsonProperty(PropertyName = "nama")]
        public string nama { get; set; }

        [JsonProperty(PropertyName = "npp")]
        public string npp { get; set; }

        [JsonProperty(PropertyName = "role")]
        public string role { get; set; }

        [JsonProperty(PropertyName = "divisi")]
        public string divisi { get; set; }

        [JsonProperty(PropertyName = "segmen")]
        public string segmen { get; set; }
    }
}
