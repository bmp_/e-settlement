using System.Collections.Generic;
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Controllers.Models
{
     public class TimeProcessData
     {
          [JsonProperty(PropertyName = "idProses")]
          public string idProses { get; set; }

          [JsonProperty(PropertyName = "namaProses")]
          public string namaProses { get; set; }

          [JsonProperty(PropertyName = "waktu")]
          public int waktu { get; set; }

          [JsonProperty(PropertyName = "namaJenisTransaksi")]
          public string namaJenisTransaksi { get; set; }

          [JsonProperty(PropertyName = "nomorUrutJenis")]
          public int nomorUrutJenis { get; set; }
     }
}