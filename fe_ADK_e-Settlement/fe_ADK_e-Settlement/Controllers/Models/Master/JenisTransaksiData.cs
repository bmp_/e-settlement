using System.Collections.Generic;
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Controllers.Models
{
     public class JenisTransaksiData
     {
          [JsonProperty(PropertyName = "idTransaksi")]
          public string idTransaksi { get; set; }

          [JsonProperty(PropertyName = "nomor")]
          public int nomor { get; set; }

          [JsonProperty(PropertyName = "jenisTransaksi")]
          public string jenisTransaksi { get; set; }
     }
}