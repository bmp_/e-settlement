using System.Collections.Generic;
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Controllers.Models
{
     public class LookupData
     {
          [JsonProperty(PropertyName = "idLookup")]
          public string idLookup { get; set; }

          [JsonProperty(PropertyName = "tipe")]
          public string tipe { get; set; }

          [JsonProperty(PropertyName = "nama")]
          public string nama { get; set; }

          [JsonProperty(PropertyName = "nilai")]
          public int nilai { get; set; }

          [JsonProperty(PropertyName = "urutan")]
          public int urutan { get; set; }

          [JsonProperty(PropertyName = "totalRows")]
          public int totalRows { get; set; }
     }
}