using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Controllers.Models
{
    public class InitMemoData
    {
        [JsonProperty(PropertyName = "nama")]
        public string nama { get; set; }

        [JsonProperty(PropertyName = "npp")]
        public string npp { get; set; }

        [JsonProperty(PropertyName = "noTlp")]
        public string noTlp { get; set; }

        [JsonProperty(PropertyName = "noHP")]
        public string noHP { get; set; }

         [JsonProperty(PropertyName = "role")]
        public string role { get; set; }

         [JsonProperty(PropertyName = "divisi")]
        public string divisi { get; set; }

         [JsonProperty(PropertyName = "segmen")]
        public string segmen { get; set; }
    }
}