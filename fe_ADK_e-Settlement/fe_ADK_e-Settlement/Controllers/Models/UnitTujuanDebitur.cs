﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fe_ADK_e_Settlement.Controllers.Models
{
    public class UnitTujuanDebitur
    {
        [JsonProperty(PropertyName = "id")]
        public int id { get; set; }

        [JsonProperty(PropertyName = "nama")]
        public string nama { get; set; }

        [JsonProperty(PropertyName = "totalRows")]
        public int totalRows { get; set; }
    }
}
