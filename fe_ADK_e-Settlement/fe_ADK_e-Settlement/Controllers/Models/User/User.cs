using System.Collections.Generic;
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Controllers.Models
{
    public class User
    {
        [JsonProperty(PropertyName = "data")]
        public List<UserData> data { get; set; }
    }
}