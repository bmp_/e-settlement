using System.Collections.Generic;
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Controllers.Models
{
     public class AdkUserData
     {
        [JsonProperty(PropertyName = "idUserADK")]
          public string idUserADK { get; set; }

          [JsonProperty(PropertyName = "npp")]
          public string npp { get; set; }

          [JsonProperty(PropertyName = "nama")]
          public string nama { get; set; }

          [JsonProperty(PropertyName = "idUnitADK")]
          public string idUnitADK { get; set; }

          [JsonProperty(PropertyName = "namaUnit")]
          public string namaUnit { get; set; }

          [JsonProperty(PropertyName = "telp")]
          public string telp { get; set; }

          [JsonProperty(PropertyName = "hp")]
          public string hp { get; set; }

          [JsonProperty(PropertyName = "tipeUnitADK")]
          public string tipeUnitADK { get; set; }

     }
}