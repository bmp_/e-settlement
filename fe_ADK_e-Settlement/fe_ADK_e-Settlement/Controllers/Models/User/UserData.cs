using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Controllers.Models
{
    public class UserData
    {
        [JsonProperty(PropertyName = "idUser")]
        public string idUser { get; set; }

        [JsonProperty(PropertyName = "npp")]
        public string npp { get; set; }

        [JsonProperty(PropertyName = "nama")]
        public string nama { get; set; }
    }
}