using System.Collections.Generic;
using Newtonsoft.Json;

namespace fe_ADK_e_Settlement.Controllers.Models
{
     public class UserResult
     {
          [JsonProperty(PropertyName = "result")]
          public User result { get; set; }
     }
}