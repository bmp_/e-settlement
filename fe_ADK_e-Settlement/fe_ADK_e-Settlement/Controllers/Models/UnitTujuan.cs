﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fe_ADK_e_Settlement.Controllers.Models
{
    public class UnitTujuan
    {
        [JsonProperty(PropertyName = "nama")]
        public string nama { get; set; }

        [JsonProperty(PropertyName = "nilai")]
        public string nilai { get; set; }
    }
}
