﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using fe_ADK_e_Settlement.Controllers.Api;
using fe_ADK_e_Settlement.Controllers.Api.Model;
using fe_ADK_e_Settlement.Controllers.Models;
using fe_ADK_e_Settlement.Controllers.Sample;
using fe_ADK_e_Settlement.Models;
using fe_ADK_e_Settlement.Utils;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace fe_ADK_e_Settlement.Controllers
{
    [Route("inputer")]
    public class InputerController : BaseController
    {
        private long GeneratedId;
        private long MIN_RANDOM = 10000000000000000;
        private long MAX_RANDOM = 99999999999999999;

        APIInputerControler apiController = new APIInputerControler();

        // GET: /<controller>/
        [HttpGet("unit-bisnis")]
        public IActionResult UnitBisnis()
        {
            ViewBag.MemoInitialization = GetInitMemo();
            ViewBag.UnitTujuan = GetUnitTujuan();
            ViewBag.Curency = GetCurrencies();

            ViewBag.JenisTransaksi = GetListJenisTransaksi();
            ViewBag.UserChecker = GetUserChecker();
            ViewBag.UserApprover = GetUserApprover();
            return View();
        }

        [HttpGet("unit-bisnis/create/retrieve-init-memo")]
        public InitMemo GetInitMemo()
        {
            InitMemo memo = new InitMemo();

            try
            {
                if (validateSession())
                {
                    APIInputerControler controller = new APIInputerControler();
                    memo = controller.initMemoAsync().Result;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[InitMemo] " + e.Message);
            }

            return memo;
        }

        [HttpPost("unit-bisnis/create/upload/{memoId}")]
        public async Task<IActionResult> FileUpload(IFormFile file, string memoId)
        {
            try
            {
                if (validateSession())
                {
                    var fileName = Path.GetFileName(file.FileName);

                    DateTime now = DateTime.Now;

                    string uploadPath = Constant.UPLOAD_FOLDER;
                    string uploadFolder = uploadPath + now.ToString("yyyyMM");
                    if (!Directory.Exists(uploadFolder))
                    {
                        Directory.CreateDirectory(uploadFolder);
                    }

                    var filePath = Path.Combine(uploadPath, uploadFolder, fileName);

                    using (var fileSystem = new FileStream(filePath, FileMode.Create))
                    {
                        await file.CopyToAsync(fileSystem);
                    }
                    UploadFile result = apiController.UploadFileAsync(filePath, memoId).Result;

                    return Ok(buildSuccessResponse(result));
                }
                else
                {
                    return Unauthorized(buildErrorResponse(401, "Unauthorized"));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[FileUpload] " + e.Message);
                return BadRequest(buildErrorResponse(400, e.Message));
            }
           
        }

        [HttpGet("unit-bisnis/create/retrieve-unit_tujuan")]
        public UnitTujuan GetUnitTujuan()
        {
            UnitTujuan result = new UnitTujuan();
            try
            {
                if (validateSession())
                {

                    APIInputerControler controller = new APIInputerControler();
                    result = controller.GetUnitTujuanAsync().Result;

                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[GetUnitTujuan] " + e.Message);
            }
            return result;
        }

        [HttpPost("unit-bisnis/create/retrieve-booking-office")]
        public IActionResult GetBookingOffice([FromBody] Pagination pagination)
        {
            try
            {
                if (validateSession())
                {
                    APIInputerControler controller = new APIInputerControler();
                    List<BookingOfficeData>result = apiController.GetBookingOfficesAsync(pagination).Result;

                    return Ok(buildSuccessResponse(result));
                }
                else
                {
                    return Unauthorized(buildErrorResponse(401, "Unauthorized"));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[GetBookingOffice] " + e.Message);
                return BadRequest(buildErrorResponse(400, e.Message));
            }

        }

        [HttpPost("unit-bisnis/create/retrieve-unit-tujuan-debitur")]
        public IActionResult GetUnitTujuanDebitur([FromBody] Pagination pagination)
        {
            try
            {
                if (validateSession())
                {
                    APIInputerControler controller = new APIInputerControler();
                    List<UnitTujuanDebitur> result = apiController.GetUnitTujuanDebiturAsync(pagination).Result;

                    return Ok(buildSuccessResponse(result));
                }
                else
                {
                    return Unauthorized(buildErrorResponse(401, "Unauthorized"));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[GetBookingOffice] " + e.Message);
                return BadRequest(buildErrorResponse(400, e.Message));
            }

        }


        [HttpGet("unit-bisnis/create/retrieve-checker")]
        public List<RetrieveAssignUserData> GetUserChecker()
        {
            List<RetrieveAssignUserData> list = new List<RetrieveAssignUserData>();
            try
            {
                if (validateSession())
                {

                    APIInputerControler controller = new APIInputerControler();
                    list = controller.GetCheckerList().Result;

                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[GetUserChecker] " + e.Message);
            }
            return list;
        }

        [HttpGet("unit-bisnis/create/retrieve-approver")]
        public List<RetrieveAssignUserData> GetUserApprover()
        {
            List<RetrieveAssignUserData> list = new List<RetrieveAssignUserData>();
            try
            {
                if (validateSession())
                {
                    APIInputerControler controller = new APIInputerControler();
                    list = controller.GetApproverList().Result;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[GetUserApprover] " + e.Message);
            }
            return list;
        }

        [HttpGet("unit-bisnis/create/get-cif-account/{noRek}")]
        public CoreOFAModel GetAccountDetails(string noRek)
        {
            CoreOFAModel details = new CoreOFAModel();
            try
            {
                if (validateSession())
                {
                    APICoreOFAController coreOFAController = new APICoreOFAController();
                    OFACoreRequest request = new OFACoreRequest();
                    request.noRek = noRek;

                    details = coreOFAController.GetAccountDetails(request).Result;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[GetAccountDetails] " + e.Message);
            }

            return details;
        }

        [HttpPost("unit-bisnis/create")]
        public IActionResult CreateMemo([FromBody] UnitBisnisRequest request)
        {
            try
            {
                if (validateSession())
                {

                    APIInputerControler controller = new APIInputerControler();
                    bool insert = apiController.CreateMemoAsync(request).Result;
                    if(insert)
                    {
                        return Ok(buildSuccessResponse());
                    }
                    else
                    {
                        return BadRequest(buildErrorResponse(400, "Bad Request"));
                    }
                }
                else
                {
                    return Unauthorized(buildErrorResponse(401, "Unauthorized"));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[GetBookingOffice] " + e.Message);
                return BadRequest(buildErrorResponse(400, e.Message));
            }

        }

        [HttpGet("retrieve-jenis-transaksi")]
        public List<JenisTransaksiData> GetListJenisTransaksi()
        {
            List<JenisTransaksiData> list = new List<JenisTransaksiData>();
            try
            {
                if (validateSession())
                {
                    try
                    {
                        APIMasterController controller = new APIMasterController();
                        list = controller.GetJenisTransaksiAsync().Result;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("[GetJenisTransaksi] " + e.Message);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[GetListJenisTransaksi] " + e.Message);
            }
            return list;
        }

        public IActionResult Adk()
        {
            return View();
        }
        public IActionResult Sbo()
        {
           
            ViewBag.Curency = GetCurrencies();

            return View();
        }

        private long GenerateRandom(long min, long max, Random random)
        {
            byte[] buf = new byte[8];
            random.NextBytes(buf);
            long randomLong = BitConverter.ToInt64(buf, 0);

            return (Math.Abs(randomLong % (max - min)) + min);
        }

        private IList<CurrencyModel> GetCurrencies()
        {
            string json = JsonSample.JSON_CURRENCY;
            var result = JsonConvert.DeserializeObject<List<CurrencyModel>>(json);
            return result;
        }

    }
}
