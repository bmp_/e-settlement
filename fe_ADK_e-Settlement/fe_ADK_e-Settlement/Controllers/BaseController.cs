using System;
using fe_ADK_e_Settlement.Controllers.Models;
using fe_ADK_e_Settlement.Utils.Helper;
using Microsoft.AspNetCore.Mvc;

namespace fe_ADK_e_Settlement.Controllers
{
    public class BaseController : Controller
    {
        public BaseController()
        {
            //Empty constructor
        }

          protected bool validateSession()
          {
               bool valid = false;

               AuthenticationResponse authSession = SessionHelper.GetObjectFromJson<AuthenticationResponse>(HttpContext.Session, "user-session");
               if (authSession != null)
               {
                    valid = true;
               }
               return valid;
          }
         protected ServiceResponse buildSuccessResponse()
        {
            ServiceResponse response = new ServiceResponse();
            response.status = 200;
            response.message = "Success";

            return response;
        }

        protected ServiceResponse buildSuccessResponse(string message)
        {
            ServiceResponse response = new ServiceResponse();
            response.status = 200;
            response.message = message;

            return response;
        }

        protected ServiceResponse buildSuccessResponse(Object obj)
        {
            ServiceResponse response = new ServiceResponse();
            response.status = 200;
            response.message = "Success";
            response.data = obj;

            return response;
        }
        protected ServiceResponse buildErrorResponse(int status, string message)
        {
            ServiceResponse response = new ServiceResponse();
            response.status = status;
            response.message = message;

            return response;
        }
    }
}