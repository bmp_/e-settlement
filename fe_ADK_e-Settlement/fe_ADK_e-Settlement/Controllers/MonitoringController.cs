﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using fe_ADK_e_Settlement.Controllers.Api;
using fe_ADK_e_Settlement.Controllers.Sample;
using fe_ADK_e_Settlement.Controllers.utils;
using fe_ADK_e_Settlement.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace fe_ADK_e_Settlement.Controllers
{
    public class MonitoringController : Controller
    {
        private readonly ILoggerManager logger;

        public MonitoringController(ILoggerManager logger)
        {
            this.logger = logger;

        }
        
        // GET: /<controller>/
        public IActionResult Index()
        {

            ViewBag.Monitoring = GetUnitBisnisList();

            return View();
        }

         public IActionResult ViewDetail()
        {
            return View();
        }

        private IList<ViewTableModel> GetUnitBisnisList()
        {
            string json = JsonSample.JSON_UNITBISNIS_CHECKER;
            var result = JsonConvert.DeserializeObject<List<ViewTableModel>>(json);
            return result;
        }

        private void Start(string methodName)
        {
            this.logger.LogInformation($"[" + methodName + "]: Started");
        }

        private void Completed(string methodName)
        {
            this.logger.LogInformation($"[" + methodName + "]: Completed");
        }
        
    }
}
