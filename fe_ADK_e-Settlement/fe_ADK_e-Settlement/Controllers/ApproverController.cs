﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using fe_ADK_e_Settlement.Controllers.Api;
using fe_ADK_e_Settlement.Controllers.Sample;
using fe_ADK_e_Settlement.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace fe_ADK_e_Settlement.Controllers
{
  public class ApproverController : Controller
  {
    public IActionResult UnitBisnis()
    {
      ViewBag.ApproverUnitBisnis = UnitBisnisList;
      return View();
    }

    public IActionResult Adk()
    {
      ViewBag.ApproverAdk = UnitBisnisList;
      return View();
    }


    public IActionResult ViewDetail()
    {
      return View();
    }



    private IList<ViewTableModel> UnitBisnisList
    {
      get
      {
        string json = JsonSample.JSON_UNITBISNIS_APPROVER;
        var result = JsonConvert.DeserializeObject<List<ViewTableModel>>(json);
        return result;
      }
    }
  }
}
