﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace fe_ADK_e_Settlement.Controllers
{
    public class DistribusiController : Controller
    {
        // GET: /<controller>/
        public IActionResult Adk()
        {
            return View();
        }
        // GET: /<controller>/
        public IActionResult Pengelola()
        {
            return View();
        }
        // GET: /<controller>/
        public IActionResult Penyelia()
        {
            return View();
        }
    }
}
