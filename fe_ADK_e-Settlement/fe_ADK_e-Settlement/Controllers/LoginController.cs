using System;
using System.Collections.Generic;
using fe_ADK_e_Settlement.Controllers.Api;
using fe_ADK_e_Settlement.Controllers.Models;
using fe_ADK_e_Settlement.Manager;
using fe_ADK_e_Settlement.Models;
using fe_ADK_e_Settlement.Utils.Helper;
using Microsoft.AspNetCore.Mvc;

namespace fe_ADK_e_Settlement.Controllers
{
    public class LoginController : BaseController
     {

          public IList<UserData> userList { get; set; }

          public IActionResult Index()
          {
               //ViewBag.UserList = getUserList();

               ViewBag.AdkUserList = GetAdkUserList();
               
               return View();
          }

          public List<AdkUserData> GetAdkUserList()
          {
               List<AdkUserData> users = new List<AdkUserData>();
               try
               {
                    APIUserController controller = new APIUserController();

                    if (users != null)
                    {
                         users = controller.GetUserAdk().Result;
                         Console.WriteLine("[GetAdkUserList] " + users.Count);
                    }
               }
               catch (Exception e)
               {
                    Console.WriteLine("[getUserList] " + e.Message);
               }
               return users;
          }


        [HttpPost]
        public IActionResult Index(AuthenticationRequest request)
        {
            var controller = new APIAuthenticationController();
            try
            {
                if (request != null)
                {
                    AuthenticationResponse response = controller.Authenticate(request.npp).Result;

                    if (response != null)
                    {
                        SessionHelper.SetObjectAsJson(HttpContext.Session, "user-session", response);
                        return Ok(buildSuccessResponse(response));
                    }
                    else
                    {
                        return Unauthorized(buildErrorResponse(401, "Unauthorized"));
                    }
                }
            }
            catch (Exception e)
            {
                return BadRequest(buildErrorResponse(400, e.Message));
            }

            return null;
        }

        [HttpPost]
          public IActionResult AuthenticateUser([FromBody] AuthenticationRequest request)
          {
               var controller = new APIAuthenticationController();
               try
               {
                    if (request != null)
                    {
                         AuthenticationResponse response = controller.Authenticate(request.npp).Result;

                         if (response != null)
                         {
                              SessionHelper.SetObjectAsJson(HttpContext.Session, "user-session", response);
                              return Ok(buildSuccessResponse(response));
                         }
                         else
                         {
                              return Unauthorized(buildErrorResponse(401, "Unauthorized"));
                         }
                    }
               }
               catch (Exception e)
               {
                    return BadRequest(buildErrorResponse(400, e.Message));
               }

               return null;
          }

          [HttpGet]
          public IActionResult Session()
          {
               try
               {
                    if (validateSession())
                    {
                         return Ok(buildSuccessResponse());
                    }
                    else
                    {
                         SessionHelper.DeleteSession(HttpContext.Session, "user-session");
                         return Unauthorized(buildErrorResponse(401, "Unauthorized"));
                    }
               }
               catch (Exception e)
               {
                    return BadRequest(buildErrorResponse(400, e.Message));
               }

          }


          [HttpPost]
          public IActionResult Logout()
          {
               try
               {
                    if (validateSession())
                    {
                         SessionHelper.DeleteSession(HttpContext.Session, "user-session");
                         return Ok(buildSuccessResponse());
                    }
                    else
                    {

                         return Unauthorized(buildErrorResponse(401, "Unauthorized"));
                    }
               }
               catch (Exception e)
               {
                    return BadRequest(buildErrorResponse(400, e.Message));
               }
          }
     }
}