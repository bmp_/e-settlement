﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using fe_ADK_e_Settlement.Controllers.Api;
using fe_ADK_e_Settlement.Controllers.Sample;
using fe_ADK_e_Settlement.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace fe_ADK_e_Settlement.Controllers
{
  public class InboxController : Controller
  {
    // GET: /<controller>/
    public IActionResult InboxPembatalan()
    {
      ViewBag.InboxPembatalan = GetUnitBisnisList();
      return View();
    }
    // GET: /<controller>/
    public IActionResult ViewDetail()
    {
      return View();
    }



    private IList<ViewTableModel> GetUnitBisnisList()
    {
      string json = JsonSample.JSON_INBOX_PEMBATALAN;
      var result = JsonConvert.DeserializeObject<List<ViewTableModel>>(json);
      return result;
    }
  }
}
